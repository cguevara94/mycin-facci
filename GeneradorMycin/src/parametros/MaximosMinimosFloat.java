/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametros;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author gabrielguevara
 */
@XmlType
public class MaximosMinimosFloat {
    
    float minimo;
    float maximo;

    public MaximosMinimosFloat() {
    }

    public MaximosMinimosFloat(float minimo, float maximo) {
        this.minimo = minimo;
        this.maximo = maximo;
    }

    public float getMinimo() {
        return minimo;
    }

    public void setMinimo(float minimo) {
        this.minimo = minimo;
    }
    
    public float getMaximo() {
        return maximo;
    }

    public void setMaximo(float maximo) {
        this.maximo = maximo;
    }
    
}
