/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametros;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author gabrielguevara
 */
@XmlType
public class MaximosMinimosInt {
    
    int minimo;
    int maximo;

    public MaximosMinimosInt() {
    }
    
    public MaximosMinimosInt(int minimo, int maximo) {
        this.minimo = minimo;
        this.maximo = maximo;
    }

    public int getMinimo() {
        return minimo;
    }

    public void setMinimo(int minimo) {
        this.minimo = minimo;
    }
    
    public int getMaximo() {
        return maximo;
    }

    public void setMaximo(int maximo) {
        this.maximo = maximo;
    }
    
}
