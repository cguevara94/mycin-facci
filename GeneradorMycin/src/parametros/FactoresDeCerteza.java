/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametros;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author gabrielguevara
 */
@XmlType
public class FactoresDeCerteza {
    
    MaximosMinimosFloat valoresParaReglas;
    MaximosMinimosFloat valoresParaEvidencias;

    public FactoresDeCerteza() {
        
    }
    
    public FactoresDeCerteza(MaximosMinimosFloat valoresParaReglas, MaximosMinimosFloat valoresParaEvidencias) {
        this.valoresParaReglas = valoresParaReglas;
        this.valoresParaEvidencias = valoresParaEvidencias;
    }

    public MaximosMinimosFloat getValoresParaReglas() {
        return valoresParaReglas;
    }

    public void setValoresParaReglas(MaximosMinimosFloat valoresParaReglas) {
        this.valoresParaReglas = valoresParaReglas;
    }

    public MaximosMinimosFloat getValoresParaEvidencias() {
        return valoresParaEvidencias;
    }

    public void setValoresParaEvidencias(MaximosMinimosFloat valoresParaEvidencias) {
        this.valoresParaEvidencias = valoresParaEvidencias;
    }
    
    
    
}
