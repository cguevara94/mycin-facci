/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametros;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author gabrielguevara
 */
@XmlType
public class ParametrosDeNivel {
    
    MaximosMinimosInt reglasPorNivel;
    MaximosMinimosInt evidenciasPorRegla;
    MaximosMinimosInt hipótesisPorRegla;
    MaximosMinimosInt evidenciasMásHipótesisPorNivel;

    public ParametrosDeNivel() {
    }

    public ParametrosDeNivel(MaximosMinimosInt reglasPorNivel, MaximosMinimosInt evidenciasPorRegla, MaximosMinimosInt hipótesisPorRegla, MaximosMinimosInt evidenciasMásHipótesisPorNivel) {
        this.reglasPorNivel = reglasPorNivel;
        this.evidenciasPorRegla = evidenciasPorRegla;
        this.hipótesisPorRegla = hipótesisPorRegla;
        this.evidenciasMásHipótesisPorNivel = evidenciasMásHipótesisPorNivel;
    }

    public MaximosMinimosInt getReglasPorNivel() {
        return reglasPorNivel;
    }

    public void setReglasPorNivel(MaximosMinimosInt reglasPorNivel) {
        this.reglasPorNivel = reglasPorNivel;
    }

    public MaximosMinimosInt getEvidenciasPorRegla() {
        return evidenciasPorRegla;
    }

    public void setEvidenciasPorRegla(MaximosMinimosInt evidenciasPorRegla) {
        this.evidenciasPorRegla = evidenciasPorRegla;
    }

    public MaximosMinimosInt getHipótesisPorRegla() {
        return hipótesisPorRegla;
    }

    public void setHipótesisPorRegla(MaximosMinimosInt hipótesisPorRegla) {
        this.hipótesisPorRegla = hipótesisPorRegla;
    }

    public MaximosMinimosInt getEvidenciasMásHipótesisPorNivel() {
        return evidenciasMásHipótesisPorNivel;
    }

    public void setEvidenciasMásHipótesisPorNivel(MaximosMinimosInt evidenciasMásHipótesisPorNivel) {
        this.evidenciasMásHipótesisPorNivel = evidenciasMásHipótesisPorNivel;
    }
    
    
}
