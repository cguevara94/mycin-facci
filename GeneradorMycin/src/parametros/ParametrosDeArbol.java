/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametros;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author gabrielguevara
 */
@XmlType
public class ParametrosDeArbol {
    
    int cantidadArboles;
    MaximosMinimosInt cantidadDeNiveles;

    public ParametrosDeArbol() {
    }
    
    public ParametrosDeArbol(int cantidadArboles, MaximosMinimosInt cantidadDeNiveles) {
        this.cantidadArboles = cantidadArboles;
        this.cantidadDeNiveles = cantidadDeNiveles;
    }
    
    public int getCantidadArboles() {
        return cantidadArboles;
    }

    public void setCantidadArboles(int cantidadArboles) {
        this.cantidadArboles = cantidadArboles;
    }

    public MaximosMinimosInt getCantidadDeNiveles() {
        return cantidadDeNiveles;
    }

    public void setCantidadDeNiveles(MaximosMinimosInt cantidadDeNiveles) {
        this.cantidadDeNiveles = cantidadDeNiveles;
    }
}
