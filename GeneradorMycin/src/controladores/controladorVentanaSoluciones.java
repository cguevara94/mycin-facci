/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.util.ArrayList;
import modelos.Arbol;
import vistas.VentanaInicial;

/**
 *
 * @author gabrielguevara
 */
public class controladorVentanaSoluciones {
    
    private static ArrayList<String> soluciones;
    
    public static void mostrarSolucion(){
        soluciones = controladorVentanaInicial.getSoluciones();
        VentanaInicial.txaSolucion.setText( soluciones.get( Integer.parseInt(VentanaInicial.txtÁrbolSolucionado.getText()) -1 ) );
    }
    
    public static void siguienteSoluciónAMostrar(){
        int arbolSiguiente = Integer.parseInt( VentanaInicial.txtÁrbolSolucionado.getText() ) + 1;
        if( arbolSiguiente <= soluciones.size() )
            VentanaInicial.txtÁrbolSolucionado.setText( Integer.toString( arbolSiguiente ) );
    }
    
    public static void anteriorSoluciónAMostrar(){
        int arbolAnterior = Integer.parseInt( VentanaInicial.txtÁrbolSolucionado.getText() ) - 1;
        if( arbolAnterior > 0 )
            VentanaInicial.txtÁrbolSolucionado.setText( Integer.toString( arbolAnterior ) );
    }
    
    public static void actualizarSeleccionPrincipal(){
        VentanaInicial.txtÁrbolAMostrar.setText(VentanaInicial.txtÁrbolSolucionado.getText());
    }
    
    public static void mostrarDatosArbol(){
        ArrayList<Arbol> arbolesMycin = controladorVentanaInicial.getArbolesMycin();
        int árbolAMostrar = Integer.parseInt( VentanaInicial.txtÁrbolSolucionado.getText() ) - 1;
        VentanaInicial.lblNiveles2.setText( Integer.toString( arbolesMycin.get(árbolAMostrar).getCantidadNiveles() ) );
        VentanaInicial.lblEvidencias2.setText( Integer.toString( arbolesMycin.get(árbolAMostrar).getCantidadEvidencias()) );
        VentanaInicial.lblHipótesis2.setText( Integer.toString( arbolesMycin.get(árbolAMostrar).getCantidadHipótesis()) );
        VentanaInicial.lblReglas2.setText( Integer.toString( arbolesMycin.get(árbolAMostrar).getCantidadReglas()) );
        double solucionHipotesisFinal = arbolesMycin.get(árbolAMostrar).getNiveles().get(0).getNodos()[0].getFactorDeCerteza();
        VentanaInicial.lblSolucion2.setText( String.format("%.3f", solucionHipotesisFinal) );
    }
    
}
