/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;
import modelos.Arbol;
import modelos.Nivel;
import modelos.Nodo;
import modelos.NodoRegla;
import otros.Calculos;
import vistas.VentanaInicial;

/**
 *
 * @author gabrielguevara
 */
public class controladorVentanaArbol extends JPanel{
    
    public static void dibujarÁrbol(){        
        if(VentanaInicial.txtÁrbolADibujar.isShowing()){
            int árbolADibujar = Integer.parseInt( VentanaInicial.txtÁrbolADibujar.getText() ) - 1;
            limpiarPanel();
            Arbol árbol = controladorVentanaInicial.getArbolesMycin().get(árbolADibujar);
            VentanaInicial.pnlGráfico.setPreferredSize(new Dimension(Calculos.obtenerXMayor(árbol) + 100, (árbol.getCantidadNiveles() * 80) + 20));
            dibujar( árbol );
        }
    }
    
    public static void siguienteArbolADibujar(){
        int arbolSiguiente = Integer.parseInt( VentanaInicial.txtÁrbolADibujar.getText() ) + 1;
        if( arbolSiguiente <= controladorVentanaInicial.getArbolesMycin().size() )
            VentanaInicial.txtÁrbolADibujar.setText( Integer.toString( arbolSiguiente ) );
    }
    
    public static void anteriorArbolADibujar(){
        int arbolAnterior = Integer.parseInt( VentanaInicial.txtÁrbolADibujar.getText() ) - 1;
        if( arbolAnterior > 0 )
            VentanaInicial.txtÁrbolADibujar.setText( Integer.toString( arbolAnterior ) );
    }
    
    public static void actualizarSeleccionPrincipal(){
        VentanaInicial.txtÁrbolAMostrar.setText(VentanaInicial.txtÁrbolADibujar.getText());
    }
    
    public static void mostrarDatosArbol(){
        ArrayList<Arbol> arbolesMycin = controladorVentanaInicial.getArbolesMycin();
        int árbolAMostrar = Integer.parseInt( VentanaInicial.txtÁrbolADibujar.getText() ) - 1;
        VentanaInicial.lblNiveles1.setText( Integer.toString( arbolesMycin.get(árbolAMostrar).getCantidadNiveles() ) );
        VentanaInicial.lblEvidencias1.setText( Integer.toString( arbolesMycin.get(árbolAMostrar).getCantidadEvidencias()) );
        VentanaInicial.lblHipótesis1.setText( Integer.toString( arbolesMycin.get(árbolAMostrar).getCantidadHipótesis()) );
        VentanaInicial.lblReglas1.setText( Integer.toString( arbolesMycin.get(árbolAMostrar).getCantidadReglas()) );
        double solucionHipotesisFinal = arbolesMycin.get(árbolAMostrar).getNiveles().get(0).getNodos()[0].getFactorDeCerteza();
        VentanaInicial.lblSolucion1.setText( String.format("%.3f", solucionHipotesisFinal) );
    }
    
    public static void limpiarPanel(){
        Graphics gráfico = VentanaInicial.pnlGráfico.getGraphics();
        gráfico.setColor(Color.white);
        gráfico.fillRect(-10, -10, VentanaInicial.pnlGráfico.getWidth()+10, VentanaInicial.pnlGráfico.getHeight()+10);
    }
    
    public static void dibujar(Arbol árbol){
        Graphics gráfico = VentanaInicial.pnlGráfico.getGraphics();
        ArrayList<Nivel>  niveles = árbol.getNiveles();
        int cantidadNiveles = niveles.size();
        for(int i = 0; i < cantidadNiveles; i+=2){
            Nivel nivelHechos = niveles.get(i);
            Nodo[] nodosHechos = nivelHechos.getNodos();
            int primeraRegla = 0;
            int ultimaRegla = 0;
            int primerHecho = 0;
            int ultimoHecho = 0;
            gráfico.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
            for(int contadoHechos = 0; contadoHechos < nodosHechos.length; contadoHechos++){
                Nodo nodoHecho = nodosHechos[contadoHechos];
                gráfico.drawOval(nodoHecho.getPosicionX(), nodoHecho.getPosicionY(), Calculos.getDiametroHechos(), Calculos.getDiametroHechos());
                gráfico.drawString(nodoHecho.getDescripción(), nodoHecho.getPosicionXDescripcion(), nodoHecho.getPosicionYDescripcion());
                if( i != cantidadNiveles - 1){
                    gráfico.drawString(String.format( nodoHecho.getNúmeroDeEntradas() < 0 ? "%.1f" : "%.3f", nodoHecho.getFactorDeCerteza()), 
                            nodoHecho.getPosicionX() - 18, nodoHecho.getPosicionY() + 2);
                }else{
                    gráfico.drawString(String.format(nodoHecho.getNúmeroDeEntradas() < 0 ? "%.1f" : "%.3f", nodoHecho.getFactorDeCerteza()), 
                            nodoHecho.getPosicionX() + Calculos.getRadioHechos(), nodoHecho.getPosicionY() + Calculos.getDiametroHechos() + 10);
                }
                if(nodoHecho.getNúmeroDeEntradas() > 0){
                    Nivel nivelReglas = niveles.get(i+1);
                    Nodo[] nodosRegla = nivelReglas.getNodos();
                    primeraRegla = ultimaRegla;
                    ultimaRegla += nodoHecho.getNúmeroDeEntradas();
                    Nivel nivelHechos2 = niveles.get(i+2);
                    Nodo[] nodosHechos2 = nivelHechos2.getNodos();
                    for(int contadorReglas = primeraRegla; contadorReglas < ultimaRegla; contadorReglas++){
                        NodoRegla nodoRegla = (NodoRegla) nodosRegla[contadorReglas];
                        primerHecho = ultimoHecho;
                        ultimoHecho += nodoRegla.getNúmeroDeEntradas();
                        gráfico.drawRect(nodoRegla.getPosicionX(), nodoRegla.getPosicionY(), Calculos.getPerimetroRegla(), Calculos.getPerimetroRegla());
                        gráfico.drawString(nodoRegla.getDescripción(), nodoRegla.getPosicionXDescripcion(), nodoRegla.getPosicionYDescripcion());
                        gráfico.drawLine(nodoRegla.getPosicionX() + Calculos.getLadoRegla(), nodoRegla.getPosicionY(), 
                        nodoHecho.getPosicionX() + Calculos.getRadioHechos(), nodoHecho.getPosicionY() + Calculos.getDiametroHechos());
                        gráfico.drawString(String.format("%.1f", nodoRegla.getFactorDeCerteza()), 
                                nodoRegla.getPosicionX() - 28, nodoRegla.getPosicionY() + Calculos.getLadoRegla() + 5);
                        if(nodoRegla.getValorDeSalida() == 0){
                            gráfico.drawString(String.format("%.0f", nodoRegla.getValorDeSalida()), 
                                nodoRegla.getPosicionX() + 2, nodoRegla.getPosicionY() - 10);
                            if( i != 0)
                            gráfico.drawLine(nodoRegla.getPosicionX() + 5, nodoRegla.getPosicionY() - 8, 
                        nodoHecho.getPosicionX() + Calculos.getRadioHechos() + 5, nodoHecho.getPosicionY() + Calculos.getDiametroHechos() + 10);
                        }else{
                            gráfico.drawString(String.format("%.3f", nodoRegla.getValorDeSalida()), 
                                nodoRegla.getPosicionX() + 2, nodoRegla.getPosicionY() - 10);
                        }
                        for(int contadorHechos2 = primerHecho; contadorHechos2 < ultimoHecho; contadorHechos2++){
                            Nodo nodoHecho2 = nodosHechos2[contadorHechos2];
                            if( nodoRegla.getNúmeroDeEntradas() > 1 && nodoRegla.getOperadorLógico() == NodoRegla.OPERADOR_AND){
                                gráfico.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 25));
                                gráfico.drawString("\u2297", nodoRegla.getPosicionX() + Calculos.getLadoRegla() - 5, nodoRegla.getPosicionY()+14+Calculos.getPerimetroRegla());
                                gráfico.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
                                gráfico.drawLine(nodoRegla.getPosicionX() + Calculos.getLadoRegla(), nodoRegla.getPosicionY() + 5 + Calculos.getPerimetroRegla(), 
                        nodoHecho2.getPosicionX() + Calculos.getRadioHechos(), nodoHecho2.getPosicionY());
                            }else{
                                gráfico.drawLine(nodoRegla.getPosicionX() + Calculos.getLadoRegla(), nodoRegla.getPosicionY() + Calculos.getPerimetroRegla(), 
                        nodoHecho2.getPosicionX() + Calculos.getRadioHechos(), nodoHecho2.getPosicionY());
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
}