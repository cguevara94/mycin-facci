/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBException;
import modelos.Arbol;
import modelos.ReglaMycin;
import otros.ArchivoDeConfiguracion;
import otros.Calculos;
import otros.Configuraciones;
import otros.Ficheros;
import otros.GeneradorArbol;
import otros.GenerarReglasMycin;
import otros.ProblemasEnCSV;
import parametros.FactoresDeCerteza;
import parametros.MaximosMinimosFloat;
import parametros.MaximosMinimosInt;
import parametros.ParametrosDeArbol;
import parametros.ParametrosDeNivel;
import vistas.FileChooser;
import vistas.VentanaInicial;

/**
 *
 * @author gabrielguevara
 */
public class controladorVentanaInicial {
    
    private static ArrayList<Arbol> arbolesMycin;
    private static ArrayList<ReglaMycin> reglasMycin;
    private static ArrayList<String> soluciones;
    private static String reglasStringMycin;
    private static String factoresDeCertezaMycin;
    
    
    
    public static void generarArboles(){
        arbolesMycin = new ArrayList<>();
        soluciones = new ArrayList<>();
        int cantidadAGenerar = (int) VentanaInicial.spnCantidadAGenerar.getValue();
        for(int i = 0; i < cantidadAGenerar; i++){
            Arbol arbol = GeneradorArbol.generarArbol();
            arbolesMycin.add(arbol);
            soluciones.add( Calculos.calcularSolucion( arbol ) );
            Calculos.calcularPuntosDeNodos(arbol, 0, 100, 20, 1);
            Calculos.ajustarMargenX(arbol);
        }
        VentanaInicial.txtÁrbolAMostrar.setText("1");
    }
    
    public static void siguienteÁrbolAMostrar(){
        int arbolSiguiente = Integer.parseInt( VentanaInicial.txtÁrbolAMostrar.getText() ) + 1;
        if( arbolSiguiente <= arbolesMycin.size() ){
            VentanaInicial.txtÁrbolAMostrar.setText( Integer.toString( arbolSiguiente ) );
            actualizarOtrasPestañas();
        }
    }
    
    public static void anteriorÁrbolAMostrar(){
        int arbolAnterior = Integer.parseInt( VentanaInicial.txtÁrbolAMostrar.getText() ) - 1;
        if( arbolAnterior > 0 ){
            VentanaInicial.txtÁrbolAMostrar.setText( Integer.toString( arbolAnterior ) );
            actualizarOtrasPestañas();
        }
    }
    
    public static void actualizarOtrasPestañas(){
        String arbolSeleccionado = VentanaInicial.txtÁrbolAMostrar.getText();
        VentanaInicial.txtÁrbolSolucionado.setText( arbolSeleccionado );
        VentanaInicial.txtÁrbolADibujar.setText( arbolSeleccionado );
        mostrarDatosArbol();
        mostrarReglasArbol();
        mostrarFactoresDeCertezaArbol();
        controladorVentanaSoluciones.mostrarSolucion();
        controladorVentanaSoluciones.mostrarDatosArbol();
        controladorVentanaArbol.mostrarDatosArbol();
        controladorVentanaArbol.dibujarÁrbol();
    }
    
    public static void mostrarReglasArbol(){
        int árbolAMostrar = Integer.parseInt( VentanaInicial.txtÁrbolAMostrar.getText() ) - 1;
        reglasMycin = GenerarReglasMycin.obtenerReglasMycin( arbolesMycin.get(árbolAMostrar) );
        reglasStringMycin = GenerarReglasMycin.obtenerTesisMycin(reglasMycin) ;
        VentanaInicial.txaReglasMycin.setText(reglasStringMycin);
    }
    
    public static void mostrarFactoresDeCertezaArbol(){
        int árbolAMostrar = Integer.parseInt( VentanaInicial.txtÁrbolAMostrar.getText() ) - 1;
        factoresDeCertezaMycin = GenerarReglasMycin.obtenerFactorezDeCerteza( arbolesMycin.get(árbolAMostrar) );
        VentanaInicial.txaFactoresCerteza.setText(factoresDeCertezaMycin);
    }
    
    public static void mostrarDatosArbol(){
        int árbolAMostrar = Integer.parseInt( VentanaInicial.txtÁrbolAMostrar.getText() ) - 1;
        VentanaInicial.lblNiveles.setText( Integer.toString( arbolesMycin.get(árbolAMostrar).getCantidadNiveles() ) );
        VentanaInicial.lblEvidencias.setText( Integer.toString( arbolesMycin.get(árbolAMostrar).getCantidadEvidencias()) );
        VentanaInicial.lblHipótesis.setText( Integer.toString( arbolesMycin.get(árbolAMostrar).getCantidadHipótesis()) );
        VentanaInicial.lblReglas.setText( Integer.toString( arbolesMycin.get(árbolAMostrar).getCantidadReglas()) );
        double solucionHipotesisFinal = arbolesMycin.get(árbolAMostrar).getNiveles().get(0).getNodos()[0].getFactorDeCerteza();
        VentanaInicial.lblSolucion.setText( String.format("%.3f", solucionHipotesisFinal) );
    }
    
    public static void obtenerParámetrosDeCración(){
        otros.CreacionArbol.setMáximoNivelesArbol( (int) VentanaInicial.spnMáximoNiveles.getValue() );
        otros.CreacionArbol.setMínimoNivelesArbol( (int) VentanaInicial.spnMínimoNiveles.getValue() );
        
        otros.CreacionNiveles.setMáximoEvidenciasRegla( (int) VentanaInicial.spnMáximoEvidencias.getValue());
        otros.CreacionNiveles.setMínimoEvidenciasRegla( (int) VentanaInicial.spnMínimoEvidencias.getValue());
        
        otros.CreacionNiveles.setMáximoHipótesisRegla( (int) VentanaInicial.spnMáximoHipótesis.getValue() );
        otros.CreacionNiveles.setMínimoHipótesisRegla( (int) VentanaInicial.spnMínimoHipótesis.getValue() );
        
        otros.CreacionNiveles.setMáximoReglasNivel( (int) VentanaInicial.spnMáximoReglas.getValue() );
        otros.CreacionNiveles.setMínimoReglasNivel( (int) VentanaInicial.spnMínimoReglas.getValue() );
        
        otros.CreacionNiveles.setMáximoEvidenciasMásHipótesisNivel( (int) VentanaInicial.spnMáximoEvidenciasHipótesis.getValue() );
        otros.CreacionNiveles.setMínimoEvidenciasMásHipótesisNivel( (int) VentanaInicial.spnMínimoEvidenciasHipótesis.getValue() );
        
        otros.CreacionNiveles.setMáximoFactorCertezaEvidencia( (float) VentanaInicial.spnMáximoFactoresEvidencias.getValue());
        otros.CreacionNiveles.setMínimoFactorCertezaEvidencia( (float) VentanaInicial.spnMínimoFactoresEvidencias.getValue());
        
        otros.CreacionNiveles.setMáximoFactorCertezaRegla( (float) VentanaInicial.spnMáximoFactoresReglas.getValue());
        otros.CreacionNiveles.setMínimoFactorCertezaRegla( (float) VentanaInicial.spnMínimoFactoresReglas.getValue());

    }
    
    public static void exportarCSV(String path){
        int c = 1;
        String problemasMycin = "";
        for(Arbol arbol : arbolesMycin){
            reglasMycin = GenerarReglasMycin.obtenerReglasMycin( arbol );
            double solucion = reglasMycin.get(reglasMycin.size() - 1).getHipótesis().getFactorDeCerteza();
            String[] CSV = ProblemasEnCSV.convertirTesisEnCSV(reglasMycin);
            problemasMycin += String.format("%d;%.3f;%s\n;;%s\n;;%s", c, solucion, CSV[0], CSV[1], CSV[2]);
            problemasMycin += "\n\n\n";
            c++;
        }
        if( ! path.substring(path.length()-4, path.length()).equalsIgnoreCase(".csv")){
            path += ".csv";
        }
        Ficheros.crearArchivoUTF8(path, "", problemasMycin);
    }
    
    public static void habilitarControles(){
        VentanaInicial.txtÁrbolAMostrar.setEnabled(true);
        VentanaInicial.btnAnterior.setEnabled(true);
        VentanaInicial.btnSiguiente.setEnabled(true);
        VentanaInicial.btnExportar.setEnabled(true);
    }

    public static void lanzarFileChoose(){
        try {
            FileChooser fileChoose = new FileChooser();
        } catch (URISyntaxException ex) {
            Logger.getLogger(controladorVentanaInicial.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(controladorVentanaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void llenarParametros() throws IOException{
        try {
            ArchivoDeConfiguracion archivoConfiguracion = new ArchivoDeConfiguracion();
            if(!archivoConfiguracion.existeArchivoDeConfiguracion()){
                guardarParametrosConfiguracion();
            }
            archivoConfiguracion.cargarConfiguracion();
            Configuraciones configuraciones = new Configuraciones();
            ParametrosDeArbol parametrosArbol = configuraciones.getParametrosArbol();
            ParametrosDeNivel parametrosNivel = configuraciones.getParametrosNivel();
            FactoresDeCerteza factoresCerteza = configuraciones.getFactoresDeCerteza();
            VentanaInicial.spnCantidadAGenerar.setValue(parametrosArbol.getCantidadArboles());
            VentanaInicial.spnMáximoNiveles.setValue(parametrosArbol.getCantidadDeNiveles().getMaximo());
            VentanaInicial.spnMínimoNiveles.setValue(parametrosArbol.getCantidadDeNiveles().getMinimo());
            VentanaInicial.spnMáximoReglas.setValue(parametrosNivel.getReglasPorNivel().getMaximo());
            VentanaInicial.spnMínimoReglas.setValue(parametrosNivel.getReglasPorNivel().getMinimo());
            VentanaInicial.spnMáximoEvidencias.setValue(parametrosNivel.getEvidenciasPorRegla().getMaximo());
            VentanaInicial.spnMínimoEvidencias.setValue(parametrosNivel.getEvidenciasPorRegla().getMinimo());
            VentanaInicial.spnMáximoHipótesis.setValue(parametrosNivel.getHipótesisPorRegla().getMaximo());
            VentanaInicial.spnMínimoHipótesis.setValue(parametrosNivel.getHipótesisPorRegla().getMinimo());
            VentanaInicial.spnMáximoEvidenciasHipótesis.setValue(parametrosNivel.getEvidenciasMásHipótesisPorNivel().getMaximo());
            VentanaInicial.spnMínimoEvidenciasHipótesis.setValue(parametrosNivel.getEvidenciasMásHipótesisPorNivel().getMinimo());
            VentanaInicial.spnMáximoFactoresReglas.setValue(factoresCerteza.getValoresParaReglas().getMaximo());
            VentanaInicial.spnMínimoFactoresReglas.setValue(factoresCerteza.getValoresParaReglas().getMinimo());
            VentanaInicial.spnMáximoFactoresEvidencias.setValue(factoresCerteza.getValoresParaEvidencias().getMaximo());
            VentanaInicial.spnMínimoFactoresEvidencias.setValue(factoresCerteza.getValoresParaEvidencias().getMinimo());
        } catch (JAXBException ex) {
            Logger.getLogger(VentanaInicial.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(VentanaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void guardarParametrosConfiguracion(){
        try {
            Configuraciones configuraciones = new Configuraciones();
            ParametrosDeArbol parametrosArbol = new ParametrosDeArbol(
                    (int)VentanaInicial.spnCantidadAGenerar.getValue(),
                    new MaximosMinimosInt((int)VentanaInicial.spnMínimoNiveles.getValue(), (int) VentanaInicial.spnMáximoNiveles.getValue()));
            ParametrosDeNivel parametrosNivel = new ParametrosDeNivel(
                    new MaximosMinimosInt((int) VentanaInicial.spnMínimoReglas.getValue(), (int) VentanaInicial.spnMáximoReglas.getValue()),
                    new MaximosMinimosInt((int) VentanaInicial.spnMínimoEvidencias.getValue(), (int) VentanaInicial.spnMáximoEvidencias.getValue()),
                    new MaximosMinimosInt((int) VentanaInicial.spnMínimoHipótesis.getValue(), (int) VentanaInicial.spnMáximoHipótesis.getValue()),
                    new MaximosMinimosInt((int) VentanaInicial.spnMínimoEvidenciasHipótesis.getValue(),
                            (int) VentanaInicial.spnMáximoEvidenciasHipótesis.getValue()));
            FactoresDeCerteza factoresCerteza = new FactoresDeCerteza(
                    new MaximosMinimosFloat( (float) VentanaInicial.spnMínimoFactoresReglas.getValue(),
                            (float) VentanaInicial.spnMáximoFactoresReglas.getValue()),
                    new MaximosMinimosFloat((float) VentanaInicial.spnMínimoFactoresEvidencias.getValue(),
                            (float) VentanaInicial.spnMáximoFactoresEvidencias.getValue()));
            configuraciones.setParametrosArbol(parametrosArbol);
            configuraciones.setParametrosNivel(parametrosNivel);
            configuraciones.setFactoresDeCerteza(factoresCerteza);
            ArchivoDeConfiguracion archivoConfiguracion = new ArchivoDeConfiguracion();
            archivoConfiguracion.guardarConfiguracion();
        } catch (JAXBException | URISyntaxException | IOException ex) {
            Logger.getLogger(controladorVentanaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void habilitarNavegación(){
        VentanaInicial.tbp_vistas.setEnabled(true);
    }
    
    public static ArrayList<Arbol> getArbolesMycin() {
        return arbolesMycin;
    }

    public static void setArbolesMycin(ArrayList<Arbol> arbolesMycin) {
        controladorVentanaInicial.arbolesMycin = arbolesMycin;
    }

    public static ArrayList<ReglaMycin> getReglasMycin() {
        return reglasMycin;
    }

    public static void setReglasMycin(ArrayList<ReglaMycin> reglasMycin) {
        controladorVentanaInicial.reglasMycin = reglasMycin;
    }

    public static ArrayList<String> getSoluciones() {
        return soluciones;
    }

    public static void setSoluciones(ArrayList<String> soluciones) {
        controladorVentanaInicial.soluciones = soluciones;
    }

    public static String getReglasStringMycin() {
        return reglasStringMycin;
    }

    public static void setReglasStringMycin(String reglasStringMycin) {
        controladorVentanaInicial.reglasStringMycin = reglasStringMycin;
    }

    public static String getFactoresDeCertezaMycin() {
        return factoresDeCertezaMycin;
    }

    public static void setFactoresDeCertezaMycin(String factoresDeCertezaMycin) {
        controladorVentanaInicial.factoresDeCertezaMycin = factoresDeCertezaMycin;
    }
    
    
    
    
}
