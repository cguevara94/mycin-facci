/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import java.util.Arrays;
import modelos.Arbol;
import modelos.Nivel;
import modelos.Nodo;

/**
 *
 * @author gabrielguevara
 */
public class ImpresionDetalles {
    
    public static void mostratDetalleArbol(Arbol arbol)
    {
        System.out.printf("Niveles: %d\nEvidencias: %d\nReglas: %d\nHipótesis: %d\n\n", arbol.getCantidadNiveles(), arbol.getCantidadEvidencias(),
                arbol.getCantidadReglas(), arbol.getCantidadHipótesis());
    }
    
    public static void mostratDetalleNiveles(Arbol arbol)
    {
        for(int i = 0; i < arbol.getCantidadNiveles(); i++)
        {
            Nivel nivel = arbol.getNiveles().get(i);
            
            System.out.printf("Evidencias: %d\nReglas: %d\nHipótesis: %d\nEvidenciasMásHipótesis: %d\n\n", nivel.getCantidadEvidencias(), 
                    nivel.getCantidadReglas(),nivel.getCantidadHipótesis(), nivel.getCantidadEvidenciasMásHipótesis());
        }
    }
    
    public static void mostratDetalleNodos(Arbol arbol)
    {
        String salida = "";
        for(int i = 0; i < arbol.getCantidadNiveles(); i++)
        {
            Nivel nivel = arbol.getNiveles().get(i);
            Nodo[] nodos = nivel.getNodos();
            
            switch( i%2 )
            {       
                case 0:
                    Nodo[] reglas = nodos;
                    for(int j = 0;j < reglas.length;j++){
                        salida += reglas[j].getDescripción()+":"+reglas[j].getNodoSaliente()+":"+reglas[j].getNúmeroDeEntradas()+"\t";
                    }
                    break;
                    
                default:
                    Nodo[] hechos = nodos;
                    for(int j = 0;j < nodos.length;j++){
                        salida += hechos[j].getDescripción()+":"+hechos[j].getNodoSaliente()+":"+hechos[j].getNúmeroDeEntradas()+"\t";
                    }
                    break;
            }
            salida += "\n";
        }
        System.out.println(salida);
    }
    
}
