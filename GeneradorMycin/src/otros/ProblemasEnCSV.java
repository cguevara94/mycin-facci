/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import java.util.ArrayList;
import modelos.Nodo;
import modelos.NodoRegla;
import modelos.ReglaMycin;

/**
 *
 * @author gabrielguevara
 */
public class ProblemasEnCSV {
    
    public static String[] convertirTesisEnCSV(ArrayList<ReglaMycin> reglasMycin){
        String reglasString = "";
        String reglasMycinString = "";
        String evidenciasString = "";
        for(ReglaMycin reglaMycin : reglasMycin){
            String[] seccionCSV = convertirEnCSV(reglaMycin);
            if( ! reglasString.equals("") ){
                reglasString += ";";
                reglasMycinString += ";";
                if( ! seccionCSV[2].equals("") )
                    evidenciasString += ";";
            }
            reglasString += seccionCSV[0];
            reglasMycinString += seccionCSV[1];
            evidenciasString += seccionCSV[2];
        }
        String[] tesisCSV = {reglasString, reglasMycinString, evidenciasString};
        return tesisCSV;
    }
    
    public static String[] convertirEnCSV(ReglaMycin reglaMycin){
        Nodo[] entradas = reglaMycin.getEntradas();
        NodoRegla regla = reglaMycin.getRegla();
        String reglaParaCSV = String.format("%s (%.1f)", regla.getDescripción(), regla.getFactorDeCerteza());
        String reglaMycinString  = GenerarReglasMycin.obtenerReglaString(reglaMycin);
        String evidenciasString = "";
        for(int contadorEntradas = 0; contadorEntradas < entradas.length; contadorEntradas++){
            Nodo nodo = entradas[contadorEntradas];
            if( nodo.getNúmeroDeEntradas() == -1 ){
                if( ! evidenciasString.equals("")){
                    evidenciasString += ";";
                }
                evidenciasString += String.format("%s (%.1f)", nodo.getDescripción(), nodo.getFactorDeCerteza());
            }
        }
        String[] reglaCSV = {reglaParaCSV, reglaMycinString, evidenciasString}; 
        return reglaCSV;
    }
    
}
