/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import java.util.ArrayList;
import java.util.Arrays;
import modelos.Arbol;
import modelos.Nivel;
import modelos.NivelHechos;
import modelos.NivelReglas;
import modelos.Nodo;
import modelos.NodoEvidencia;
import modelos.NodoHipotesis;
import modelos.NodoRegla;

/**
 *
 * @author gabrielguevara
 */
public class CreacionNiveles {
    
    private static int máximoEvidenciasMásHipótesisNivel = 5;
    private static int mínimoEvidenciasMásHipótesisNivel = 3;
    
    private static int máximoReglasNivel = 5;
    private static int mínimoReglasNivel = 3;
    private static int máximoEvidenciasRegla = 3;
    private static int mínimoEvidenciasRegla = 0;
    private static int máximoHipótesisRegla = 3;
    private static int mínimoHipótesisRegla = 0;
    
    private static float máximoFactorCertezaEvidencia = 1;
    private static float mínimoFactorCertezaEvidencia = -1;
    private static float máximoFactorCertezaRegla = 1;
    private static float mínimoFactorCertezaRegla = (float) -0.5;
    
    
    public static void crearNivelesArbol(Arbol arbol)
    {
        ArrayList<Nivel> niveles = arbol.getNiveles();
        Nivel nivel;
        nivel = crearNivelHipótesisFinal();
        niveles.add(nivel);
        for(int i = 1; i < arbol.getCantidadNiveles()-1; i++)
        {
            if( i % 2 == 0){
                nivel = nuevoNivelHechos(arbol, i-1);//Fin if
            }
            else{
                if( i == arbol.getCantidadNiveles()-2){
                    nivel = nuevoNivelReglas(arbol, i-1, true);
                }
                else
                {
                    nivel = nuevoNivelReglas(arbol, i-1, false);
                }
            }
            niveles.add(nivel);
        }
        nivel = nuevoNivelEvidencias(arbol);
        niveles.add(nivel);
    }
    
    public static Nivel nuevoNivelReglas(Arbol arbol, int pisoAnterior, boolean soloEvidencias)
    {
        Nivel nivelNuevo;
        Nivel nivelAnterior = arbol.getNiveles().get(pisoAnterior);
        int amplitudNivelAnterior = nivelAnterior.getCantidadHipótesis();
        int iteraciones = 0;
        do{
            nivelNuevo = new NivelReglas(máximoReglasNivel, mínimoReglasNivel);
            iteraciones++;
            if(iteraciones == 100) {ValidacionCreaciones.setSeDebeDescartar(true); break;}
            else{ ValidacionCreaciones.setSeDebeDescartar(false); }
        }while( nivelNuevo.getCantidadReglas() < amplitudNivelAnterior);
        int cantidadReglas = nivelNuevo.getCantidadReglas();
        Nodo[] nodosRegla = crearNodosReglaNivel(cantidadReglas, soloEvidencias);
        Nodo[] nodosHipótesis = nivelAnterior.getNodos();
        nodosRegla = conectarReglasAHipótesis(nodosRegla, nodosHipótesis, nivelAnterior.getCantidadHipótesis());
        nivelNuevo.setNodos( nodosRegla );
        return nivelNuevo;
    }
    
    private static int[] obtenerPosiciónHipótesis( Nodo[] nodos, int cantidadHipótesis){
        int cantidadNodos = nodos.length;
        int posiciónDeGuardado = 0;
        int[] posiciónHipótesis = new int[cantidadHipótesis];
        for(int i = 0; i < cantidadNodos; i++){
            if(nodos[i].getNúmeroDeEntradas() > -1){
                posiciónHipótesis[posiciónDeGuardado] = i;
                posiciónDeGuardado++;
            }
        }
        return posiciónHipótesis;
    }
    
    private static Nodo[] crearNodosReglaNivel(int cantidadReglas, boolean soloEvidencias){
        Nodo[] nodos = null;
        int iteraciones = 0;
        int totalEvidencias;
        int totalHipótesis;
        do{
            int máximoHipótesisPosibles = máximoReglasNivel;
            int máximoEvidenciasPosibles = máximoReglasNivel;
            int cantidadReglasSinSubNodos = cantidadReglas;
            int máximoHipótesis = 0;
            int máximoEvidencias = 0;
            totalEvidencias = 0;
            totalHipótesis = 0;
            nodos = new Nodo[cantidadReglas];
            for(int i = 0; i < cantidadReglas; i++){
                int canditdadEvidencias = 0;
                int canditdadHipótesis = 0;
                    máximoEvidencias = Math.min(máximoEvidenciasRegla, ( ( máximoEvidenciasPosibles - cantidadReglasSinSubNodos ) + 1));
                    if(!soloEvidencias){
                        máximoHipótesis = Math.min(máximoHipótesisRegla, ( ( máximoHipótesisPosibles - cantidadReglasSinSubNodos ) + 1));
                        do{
                            canditdadHipótesis = Operaciones.obtenerUnValorEntreLimites(máximoHipótesis, mínimoHipótesisRegla);
                            canditdadEvidencias = Operaciones.obtenerUnValorEntreLimites(máximoEvidencias, mínimoEvidenciasRegla);
                        }while( (canditdadHipótesis + canditdadEvidencias) < 1 || canditdadHipótesis < 1 );
                    }else{
                        canditdadEvidencias = Operaciones.obtenerUnValorEntreLimites(máximoEvidencias, 1);
                    }
                    iteraciones++;
                    if(iteraciones == 100) {ValidacionCreaciones.setSeDebeDescartar(true); break;}
                    else{ ValidacionCreaciones.setSeDebeDescartar(false); }
                máximoHipótesisPosibles -= máximoHipótesis;
                máximoEvidenciasPosibles -= máximoEvidencias;
                cantidadReglasSinSubNodos--;
                totalEvidencias += canditdadEvidencias;
                totalHipótesis += canditdadHipótesis;
                nodos[i] = new NodoRegla(máximoFactorCertezaRegla, mínimoFactorCertezaRegla,
                        canditdadEvidencias, canditdadHipótesis, Operaciones.sortearAND_OR());
            }
        }while( ( (totalEvidencias + totalHipótesis) > máximoEvidenciasMásHipótesisNivel 
                    || (totalEvidencias + totalHipótesis) < mínimoEvidenciasMásHipótesisNivel)
                && totalHipótesis == 0);
        return nodos;
    }
    
    private static Nodo[] conectarReglasAHipótesis(Nodo[] nodosRegla, Nodo[]nodosHipótesis, int cantidadHipótesis){
        int cantidadReglasSinConectar;
        int cantidadHipótesisSinConectar;
        int iteraciones = 0;
        do{
            cantidadHipótesisSinConectar = cantidadHipótesis;
            cantidadReglasSinConectar = nodosRegla.length;
            int ultimaReglaConectada = 0;
            for(int i = 0; i < nodosHipótesis.length; i++){
                if(nodosHipótesis[i].getNúmeroDeEntradas() > -1){
                    int conexionesPermitidas = cantidadReglasSinConectar-(cantidadHipótesisSinConectar-1);
                    int cantidadConexiones = Operaciones.obtenerUnValorEntreLimites(conexionesPermitidas, 1);
                    int ultimaReglaAConectar = (ultimaReglaConectada + cantidadConexiones);
                    for(int j = ultimaReglaConectada; j < ultimaReglaAConectar; j++){
                        nodosRegla[j].setNodoSaliente(i);
                        ultimaReglaConectada = j+1;
                    }
                    cantidadHipótesisSinConectar--;
                    cantidadReglasSinConectar -= cantidadConexiones;
                    nodosHipótesis[i].setNúmeroDeEntradas( cantidadConexiones );
                }
            }
            iteraciones++;
            if(iteraciones == 100) {ValidacionCreaciones.setSeDebeDescartar(true); break;}
            else{ ValidacionCreaciones.setSeDebeDescartar(false); }
        }while(cantidadReglasSinConectar > 0 || cantidadHipótesisSinConectar > 0);
        return nodosRegla;
    }
    
    public static Nivel nuevoNivelHechos(Arbol arbol, int pisoAnterior)
    {
        Nivel nivelNuevo;
        NivelReglas nivelAnterior = (NivelReglas) arbol.getNiveles().get(pisoAnterior);
        int amplitudNivelAnterior = nivelAnterior.getCantidadReglas();
        int iteraciones = 0;
        int cantidadEvidenciasTotales = 0;
        int cantidadHipótesisTotales = 0;
        Nodo[] nodosHechosNivel = new Nodo[0];
        NodoRegla[] nodosRegla = Arrays.copyOf(nivelAnterior.getNodos(), nivelAnterior.getNodos().length, NodoRegla[].class);
        for(int i = 0; i < amplitudNivelAnterior; i++){
            int cantidadEvidencias = nodosRegla[i].getNúmeroDeEvidencias();
            int cantidadHipótesis = nodosRegla[i].getNúmeroDeHipótesis();
            cantidadEvidenciasTotales += cantidadEvidencias;
            cantidadHipótesisTotales += cantidadHipótesis;
            NodoEvidencia[] nodosEvidencia = crearEvidenciasRegla(cantidadEvidencias, i);
            NodoHipotesis[] nodosHipótesis = crearHiportesisRegla(cantidadHipótesis, i);
            Nodo[] nodosHechosRegla = ManipulacionNodos.unirNodosAleatoriamente(nodosEvidencia, nodosHipótesis);
            nodosHechosNivel = ManipulacionNodos.unirNodos(nodosHechosNivel, nodosHechosRegla);
        }
        nivelNuevo = new NivelHechos(cantidadEvidenciasTotales, cantidadHipótesisTotales);
        nivelNuevo.setNodos(nodosHechosNivel);
        return nivelNuevo;
    }
    
    private static NodoEvidencia[] crearEvidenciasRegla(int cantidadEvidencias, int reglaSaliente){
        NodoEvidencia[] nodosEvidencia = new NodoEvidencia[cantidadEvidencias];
        for(int i = 0; i < cantidadEvidencias; i++){
            nodosEvidencia[i] = new NodoEvidencia(máximoFactorCertezaEvidencia, mínimoFactorCertezaEvidencia);
            nodosEvidencia[i].setNodoSaliente(reglaSaliente);
        }
        return nodosEvidencia;
    }
    
    private static NodoHipotesis[] crearHiportesisRegla(int cantidadHipótesis, int reglaSaliente){
        NodoHipotesis[] nodosHipótesis = new NodoHipotesis[cantidadHipótesis];
        for(int i = 0; i < cantidadHipótesis; i++){
            nodosHipótesis[i] = new NodoHipotesis();
            nodosHipótesis[i].setNodoSaliente(reglaSaliente);
        }
        return nodosHipótesis;
    }
    
    public static Nivel nuevoNivelEvidencias(Arbol arbol)
    {
        Nivel nivelNuevo;
        Nivel nivelAnterior = arbol.getNiveles().get(arbol.getCantidadNiveles()-2);
        int amplitudNivelAnterior = nivelAnterior.getCantidadReglas();
        int cantidadEvidenciasTotales = 0;
        Nodo[] nodosHechosNivel = new Nodo[0];
        NodoRegla[] nodosRegla = Arrays.copyOf(nivelAnterior.getNodos(), nivelAnterior.getNodos().length, NodoRegla[].class);
        for(int i = 0; i < amplitudNivelAnterior; i++){
            int cantidadEvidencias = nodosRegla[i].getNúmeroDeEvidencias();
            cantidadEvidenciasTotales += cantidadEvidencias;
            NodoEvidencia[] nodosEvidencia = crearEvidenciasRegla(cantidadEvidencias, i);
            nodosHechosNivel = ManipulacionNodos.unirNodos(nodosHechosNivel, nodosEvidencia);
        }
        nivelNuevo = new NivelHechos(cantidadEvidenciasTotales, 0);
        nivelNuevo.setNodos(nodosHechosNivel);
        return nivelNuevo;
    }
    
    public static Nivel crearNivelHipótesisFinal()
    {
        Nivel nivel = new NivelHechos(0, 1);
        NodoHipotesis[] nodos = new NodoHipotesis[1];
        nodos[0] = new NodoHipotesis();
        nivel.setNodos( nodos );
        return nivel;
    }

    public static int getMáximoEvidenciasMásHipótesisNivel() {
        return máximoEvidenciasMásHipótesisNivel;
    }

    public static void setMáximoEvidenciasMásHipótesisNivel(int máximoEvidenciasMásHipótesisNivel) {
        CreacionNiveles.máximoEvidenciasMásHipótesisNivel = máximoEvidenciasMásHipótesisNivel;
    }

    public static int getMínimoEvidenciasMásHipótesisNivel() {
        return mínimoEvidenciasMásHipótesisNivel;
    }

    public static void setMínimoEvidenciasMásHipótesisNivel(int mínimoEvidenciasMásHipótesisNivel) {
        CreacionNiveles.mínimoEvidenciasMásHipótesisNivel = mínimoEvidenciasMásHipótesisNivel;
    }

    public static int getMáximoReglasNivel() {
        return máximoReglasNivel;
    }

    public static void setMáximoReglasNivel(int máximoReglasNivel) {
        CreacionNiveles.máximoReglasNivel = máximoReglasNivel;
    }

    public static int getMínimoReglasNivel() {
        return mínimoReglasNivel;
    }

    public static void setMínimoReglasNivel(int mínimoReglasNivel) {
        CreacionNiveles.mínimoReglasNivel = mínimoReglasNivel;
    }    

    public static int getMáximoEvidenciasRegla() {
        return máximoEvidenciasRegla;
    }

    public static void setMáximoEvidenciasRegla(int máximoEvidenciasRegla) {
        CreacionNiveles.máximoEvidenciasRegla = máximoEvidenciasRegla;
    }

    public static int getMínimoEvidenciasRegla() {
        return mínimoEvidenciasRegla;
    }

    public static void setMínimoEvidenciasRegla(int mínimoEvidenciasRegla) {
        CreacionNiveles.mínimoEvidenciasRegla = mínimoEvidenciasRegla;
    }

    public static int getMáximoHipótesisRegla() {
        return máximoHipótesisRegla;
    }

    public static void setMáximoHipótesisRegla(int máximoHipótesisRegla) {
        CreacionNiveles.máximoHipótesisRegla = máximoHipótesisRegla;
    }

    public static int getMínimoHipótesisRegla() {
        return mínimoHipótesisRegla;
    }

    public static void setMínimoHipótesisRegla(int mínimoHipótesisRegla) {
        CreacionNiveles.mínimoHipótesisRegla = mínimoHipótesisRegla;
    }

    public static double getMáximoFactorCertezaEvidencia() {
        return máximoFactorCertezaEvidencia;
    }

    public static void setMáximoFactorCertezaEvidencia(float máximoFactorCertezaEvidencia) {
        CreacionNiveles.máximoFactorCertezaEvidencia = máximoFactorCertezaEvidencia;
    }

    public static double getMínimoFactorCertezaEvidencia() {
        return mínimoFactorCertezaEvidencia;
    }

    public static void setMínimoFactorCertezaEvidencia(float mínimoFactorCertezaEvidencia) {
        CreacionNiveles.mínimoFactorCertezaEvidencia = mínimoFactorCertezaEvidencia;
    }

    public static double getMáximoFactorCertezaRegla() {
        return máximoFactorCertezaRegla;
    }

    public static void setMáximoFactorCertezaRegla(float máximoFactorCertezaRegla) {
        CreacionNiveles.máximoFactorCertezaRegla = máximoFactorCertezaRegla;
    }

    public static double getMínimoFactorCertezaRegla() {
        return mínimoFactorCertezaRegla;
    }

    public static void setMínimoFactorCertezaRegla(float mínimoFactorCertezaRegla) {
        CreacionNiveles.mínimoFactorCertezaRegla = mínimoFactorCertezaRegla;
    }
    
    
}
