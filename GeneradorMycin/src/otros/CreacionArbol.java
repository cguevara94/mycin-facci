/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import modelos.Arbol;

/**
 *
 * @author gabrielguevara
 */
public class CreacionArbol {
    
    private static int númeroNivelesArbol;
    private static int númeroReglasArbol;
    private static int númeroEvidenciasArbol;
    private static int númeroHipótesisArbol;
    
    private static int máximoNivelesArbol;
    private static int mínimoNivelesArbol;

    public static Arbol crearArbol()
    {
        máximoNivelesArbol = Configuraciones.parametrosArbol.getCantidadDeNiveles().getMaximo();
        mínimoNivelesArbol = Configuraciones.parametrosArbol.getCantidadDeNiveles().getMinimo();
        definirAlturaArbol();
        return new Arbol(númeroNivelesArbol);
    }
    
    public static void definirAlturaArbol()
    {
        do
        {
            númeroNivelesArbol = Operaciones.obtenerUnValorEntreLimites(máximoNivelesArbol, mínimoNivelesArbol);
        }while(númeroNivelesArbol % 2 == 0);
    }

    public static int getNúmeroNivelesArbol() {
        return númeroNivelesArbol;
    }

    public static void setNúmeroNivelesArbol(int númeroNivelesArbol) {
        CreacionArbol.númeroNivelesArbol = númeroNivelesArbol;
    }

    public static int getNúmeroReglasArbol() {
        return númeroReglasArbol;
    }

    public static void setNúmeroReglasArbol(int númeroReglasArbol) {
        CreacionArbol.númeroReglasArbol = númeroReglasArbol;
    }

    public static int getNúmeroEvidenciasArbol() {
        return númeroEvidenciasArbol;
    }

    public static void setNúmeroEvidenciasArbol(int númeroEvidenciasArbol) {
        CreacionArbol.númeroEvidenciasArbol = númeroEvidenciasArbol;
    }

    public static int getNúmeroHipótesisArbol() {
        return númeroHipótesisArbol;
    }

    public static void setNúmeroHipótesisArbol(int númeroHipótesisArbol) {
        CreacionArbol.númeroHipótesisArbol = númeroHipótesisArbol;
    }

    public static int getMáximoNivelesArbol() {
        return máximoNivelesArbol;
    }

    public static void setMáximoNivelesArbol(int máximoNivelesArbol) {
        CreacionArbol.máximoNivelesArbol = máximoNivelesArbol;
    }

    public static int getMínimoNivelesArbol() {
        return mínimoNivelesArbol;
    }

    public static void setMínimoNivelesArbol(int mínimoNivelesArbol) {
        CreacionArbol.mínimoNivelesArbol = mínimoNivelesArbol;
    }
    
}