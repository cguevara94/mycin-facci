/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

/**
 *
 * @author gabrielguevara
 */
public class MedidasGUI {
    
    static Toolkit tk = Toolkit.getDefaultToolkit();
    
    public static ImageIcon icono(Class<?> clase, String icon)
    {
        return icono(clase, icon, 30, 35);
    }
    
    public static ImageIcon icono(Class<?> clase, String icon, int ancho, int alto)
    {
        ImageIcon icono = new javax.swing.ImageIcon(clase.getResource(icon));
        Image imagen = icono.getImage();
        ImageIcon iconoEscalado = new ImageIcon (imagen.getScaledInstance(ancho,alto,Image.SCALE_SMOOTH));
        return iconoEscalado;
    }
    
    public static int getAnchoPantalla(){  
        int ancho = ((int) tk.getScreenSize().getWidth());
        return ancho;
    }
    
    public static int getAltoPantalla(){  
        int alto = ((int) tk.getScreenSize().getHeight());   
        return alto;
    }
    
    public static int centrarEnX(int anchoVentana){
        int x = (getAnchoPantalla()/2)-(anchoVentana/2);
        return x;
    }
    
    public static int centrarEnY(int altoVentana){
        int y = (getAltoPantalla()/2)-(altoVentana/2);
        return y;
    }
    
}
