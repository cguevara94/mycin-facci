/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import modelos.Arbol;
import modelos.Nivel;
import modelos.NivelHechos;
import modelos.NivelReglas;
import modelos.Nodo;
import modelos.NodoRegla;

/**
 *
 * @author gabrielguevara
 */
public class Calculos {
    
    static String formula1 = "";
    static String formula2 = "";
    static String formula3 = "";
    static int diametroHechos = 50;
    static int radioHechos = diametroHechos/2;
    static int perimetroRegla = 30;
    static int ladoRegla = perimetroRegla/2;
    static int separacionHechos = diametroHechos + 20;
    static int diferenciaDiametroPerimetro = (diametroHechos - perimetroRegla);
    static int separacionReglas = perimetroRegla + diferenciaDiametroPerimetro;
    
    public static String calcularSolucion(Arbol arbol){
        ArrayList<Nivel> niveles = arbol.getNiveles();
        String solucionPasoAPaso = "";
        int cantidadNiveles = niveles.size();
        for(int contadorNiveles = cantidadNiveles-3; contadorNiveles >= 0; contadorNiveles-=2){
            NivelHechos nivelHipótesis = (NivelHechos) niveles.get(contadorNiveles);
            Nodo[] nodosHipotesis = nivelHipótesis.getNodos();
            NivelReglas nivelReglas = (NivelReglas) niveles.get(contadorNiveles + 1);
            Nodo[] nodosRegla = nivelReglas.getNodos();
            NivelHechos nivelHechos = (NivelHechos) niveles.get(contadorNiveles + 2);
            Nodo[] nodosHechos = nivelHechos.getNodos();
            int primeraRegla = 0;
            int ultimaRegla = 0;
            int primerHecho = 0;
            int ultimoHecho = 0;
            for(int contadorHipotesis = 0; contadorHipotesis < nodosHipotesis.length; contadorHipotesis++){
                Nodo nodoHipotesis = nodosHipotesis[contadorHipotesis];
                if(nodoHipotesis.getNúmeroDeEntradas() != -1){
                    primeraRegla = ultimaRegla;
                    ultimaRegla += nodoHipotesis.getNúmeroDeEntradas();
                    for(int contadorReglas = primeraRegla; contadorReglas < ultimaRegla; contadorReglas++){
                        NodoRegla nodoRegla = (NodoRegla) nodosRegla[contadorReglas];
                        primerHecho = ultimoHecho;
                        ultimoHecho += nodoRegla.getNúmeroDeEntradas();
                        Nodo hechoEntrante = nodosHechos[primerHecho];
                        String hechosEnLaRegla = 
                                hechoEntrante.getNúmeroDeEntradas() < 0 ? 
                                String.format("%s (%.1f)", hechoEntrante.getDescripción(),hechoEntrante.getFactorDeCerteza()) : 
                                String.format("%s (%.3f)", hechoEntrante.getDescripción(),hechoEntrante.getFactorDeCerteza());
                        String operacionRegla = "";
                        for(int contadorHechos = primerHecho+1; contadorHechos < ultimoHecho; contadorHechos++){
                            Nodo hecho = nodosHechos[contadorHechos];
                            if(nodoRegla.getOperadorLógico() == NodoRegla.OPERADOR_AND){
                                hechoEntrante = nodoFCMenor(hechoEntrante, hecho);
                                hechosEnLaRegla += hecho.getNúmeroDeEntradas() < 0 ? 
                                        String.format(" ∧ %s (%.1f)", hecho.getDescripción(),hecho.getFactorDeCerteza()) : 
                                        String.format(" ∧ %s (%.3f)", hecho.getDescripción(),hecho.getFactorDeCerteza());
                                operacionRegla = "Aplicación del mínimo: ";
                            }else{
                                hechoEntrante = nodoFCMayor(hechoEntrante, hecho);
                                hechosEnLaRegla += hecho.getNúmeroDeEntradas() < 0 ? 
                                        String.format(" ∨ %s (%.1f)", hecho.getDescripción(), hecho.getFactorDeCerteza()) : 
                                        String.format(" ∨ %s (%.3f)", hecho.getDescripción(), hecho.getFactorDeCerteza());
                                operacionRegla = "Aplicación del Máximo: ";
                            }
                        }
                        double valorSalidaRegla = calcularSalidaDeRegla(nodoRegla, hechoEntrante);
                        nodoRegla.setValorDeSalida( valorSalidaRegla );
                        solucionPasoAPaso = hechoEntrante.getNúmeroDeEntradas() < 0 ? 
                                String.format("%s\n%s(%.1f):\n%s%s resultado: %.1f\nValor de salida = %s (%.1f) x %s (%.1f) = %.3f\n", 
                                solucionPasoAPaso, nodoRegla.getDescripción(), nodoRegla.getFactorDeCerteza()
                                , operacionRegla, hechosEnLaRegla, hechoEntrante.getFactorDeCerteza(),
                                nodoRegla.getDescripción(), nodoRegla.getFactorDeCerteza(), 
                                hechoEntrante.getDescripción(), hechoEntrante.getFactorDeCerteza(), valorSalidaRegla) :
                                String.format("%s\n%s(%.1f):\n%s%s resultado: %.1f\nValor de salida = %s (%.1f) x %s (%.3f) = %.3f\n", 
                                solucionPasoAPaso, nodoRegla.getDescripción(), nodoRegla.getFactorDeCerteza()
                                , operacionRegla, hechosEnLaRegla, hechoEntrante.getFactorDeCerteza(),
                                nodoRegla.getDescripción(), nodoRegla.getFactorDeCerteza(), 
                                hechoEntrante.getDescripción(), hechoEntrante.getFactorDeCerteza(), valorSalidaRegla);
                    }
                    formula1 = "";
                    formula2 = "";
                    formula3 = "";
                    nodoHipotesis.setFactorDeCerteza( calcularFactorCertezaHipotesis(nodosRegla, primeraRegla, ultimaRegla) );
                    solucionPasoAPaso = String.format("%s\nValor de %s = %.3f\nFormulas Aplicadas: %s %s %s\n", 
                            solucionPasoAPaso, nodoHipotesis.getDescripción(), nodoHipotesis.getFactorDeCerteza(), 
                            formula1, formula2, formula3);
                }
            }
        }
        return solucionPasoAPaso;
    }//Fin método calcular solución
    
    private static Nodo nodoFCMenor(Nodo nodoA, Nodo nodoB){
        return nodoA.getFactorDeCerteza() < nodoB.getFactorDeCerteza() ? nodoA : nodoB;
    }
    
    private static Nodo nodoFCMayor(Nodo nodoA, Nodo nodoB){
        return nodoA.getFactorDeCerteza() > nodoB.getFactorDeCerteza() ? nodoA : nodoB;
    }
    
    private static double calcularSalidaDeRegla(Nodo nodoRegla, Nodo nodoHecho){
        double factorCerteza = 0;
        if( nodoHecho.getNúmeroDeEntradas() == -1 && nodoHecho.getFactorDeCerteza() <= 0){
            factorCerteza = 0;
        }else{
            factorCerteza = ( nodoHecho.getFactorDeCerteza() * nodoRegla.getFactorDeCerteza() );
        }
        return factorCerteza;
    }
    
    private static double calcularFactorCertezaHipotesis(Nodo[] nodosRegla, int primerRegla, int ultimaRegla){
        double factorCertezaHipotesis = ( (NodoRegla) nodosRegla[primerRegla]).getValorDeSalida();
        for(int i = primerRegla+1; i < ultimaRegla; i++){
            NodoRegla nodoRegla = (NodoRegla)nodosRegla[i];
            double factorCertezaRegla = nodoRegla.getValorDeSalida();
            if(factorCertezaHipotesis >= 0 && factorCertezaRegla >= 0){
                factorCertezaHipotesis = ((factorCertezaHipotesis + factorCertezaRegla) - (factorCertezaHipotesis * factorCertezaRegla));
                formula1 = "Formula 1";
            }else{
                if(factorCertezaHipotesis <= 0 && factorCertezaRegla <= 0){
                    factorCertezaHipotesis = ((factorCertezaHipotesis + factorCertezaRegla) + (factorCertezaHipotesis * factorCertezaRegla));
                    formula2 = "Formula 2";
                }else{
                    factorCertezaHipotesis = ( (factorCertezaHipotesis + factorCertezaRegla) / 
                            ( 1 - Math.min( Math.abs(factorCertezaHipotesis), Math.abs(factorCertezaRegla) ) ) );
                    formula3 = "Formula 3";
                }
            }
        }
        return factorCertezaHipotesis;
    }
    
    public static int obtenerXMayor(Arbol árbol){
        ArrayList<Nivel>  niveles = árbol.getNiveles();
        int cantidadNiveles = niveles.size();
        Nivel nivelInicial = niveles.get(0);
        Nodo[] nodosNivel = nivelInicial.getNodos();
        int margenMayor = nodosNivel[nodosNivel.length-1].getPosicionX();
        int margenAAumentar = 0;
        for(int contadorNiveles = 0; contadorNiveles < cantidadNiveles; contadorNiveles++){
            nodosNivel = niveles.get(contadorNiveles).getNodos();
            int margen = nodosNivel[nodosNivel.length-1].getPosicionX();
            if( margen  > margenMayor){
                margenMayor = margen;
            }
        }
        return margenMayor;
    }
    
    public static void ajustarMargenX(Arbol árbol){
        ArrayList<Nivel>  niveles = árbol.getNiveles();
        int cantidadNiveles = niveles.size();
        Nivel nivelInicial = niveles.get(0);
        int margenMenor = nivelInicial.getNodos()[0].getPosicionX();
        int margenAAumentar = 0;
        for(int contadorNiveles = 0; contadorNiveles < cantidadNiveles; contadorNiveles++){
            int margen = niveles.get(contadorNiveles).getNodos()[0].getPosicionX();
            if( margen  < margenMenor){
                margenMenor = margen;
            }
        }
        if(margenMenor < 20){
            margenAAumentar = Math.abs(margenMenor) + 20;
            for(int contadorNiveles = 0; contadorNiveles < cantidadNiveles; contadorNiveles++){
                Nodo[] nodosNivel = niveles.get(contadorNiveles).getNodos();
                for( int contadorNodos = 0; contadorNodos < nodosNivel.length; contadorNodos++ ){
                    Nodo nodo = nodosNivel[contadorNodos];
                    nodo.setPosicionX( nodo.getPosicionX() + margenAAumentar );
                    nodo.setPosicionXDescripcion( nodo.getPosicionXDescripcion() + margenAAumentar );
                }
            }
        }
    }
    
    public static int calcularPuntosDeNodos(Arbol árbol, int nivelInicial, int margenX, int margenY, int cantidadHechos){
        ArrayList<Nivel>  niveles = árbol.getNiveles();
        if( nivelInicial < niveles.size() ){
            Nivel nivelHechos1 = niveles.get(nivelInicial);
            Nodo[] nodosHechos1 = nivelHechos1.getNodos();
            int primeraRegla = 0;
            int ultimaRegla = 0;
            int posicionDeReglaEnX = 0;
            int primerNodo = 0;
            int retrocesoHechoEnX = ( ( ( ( cantidadHechos -1 ) * separacionHechos) / 2 ));
            while( nodosHechos1[primerNodo].getPosicionX() != 0 ){
                if(primerNodo < nodosHechos1.length){
                    primerNodo++;
                }else{
                    primerNodo = 0;
                    break;
                }
            }
            for(int contadorNodos = primerNodo; contadorNodos < (primerNodo + cantidadHechos); contadorNodos++){
                Nodo nodoHecho1 = nodosHechos1[contadorNodos];
                int cantidadDeReglas = nodoHecho1.getNúmeroDeEntradas();

                if( cantidadDeReglas != -1 ){
                    Nivel nivelReglas = niveles.get(nivelInicial + 1);
                    Nodo[] nodosReglas = nivelReglas.getNodos();
                    while( nodosReglas[primeraRegla].getPosicionX() != 0 ){
                        if( primeraRegla < nodosReglas.length){
                            primeraRegla++;
                        }else{
                            primeraRegla = 0;
                            break;
                        }
                    }
                    ultimaRegla = primeraRegla + cantidadDeReglas;
                    int retrocesoReglaEnX = ( ( ( cantidadDeReglas -1 ) * separacionReglas) / 2 );
                    margenX += retrocesoReglaEnX;
                    posicionDeReglaEnX = ( margenX - retrocesoHechoEnX - retrocesoReglaEnX) + (diferenciaDiametroPerimetro / 2);
                    for(int contadorReglas = primeraRegla; contadorReglas < ultimaRegla; contadorReglas++)
                    {
                        Nodo nodoRegla = nodosReglas[contadorReglas];
                        nodoRegla.setPosicionX(posicionDeReglaEnX);
                        nodoRegla.setPosicionY(margenY + 80);
                        nodoRegla.setPosicionXDescripcion((int) (posicionDeReglaEnX + (perimetroRegla / 2.6)));
                        nodoRegla.setPosicionYDescripcion((int) (margenY + 80 + (perimetroRegla / 1.6)));
                        posicionDeReglaEnX = 
                                calcularPuntosDeNodos(árbol, nivelInicial + 2, posicionDeReglaEnX, margenY + 140, nodoRegla.getNúmeroDeEntradas())
                                + separacionReglas;
                    }
                    int posX1 = nodosReglas[primeraRegla].getPosicionX();
                    int posX2 = nodosReglas[ultimaRegla - 1].getPosicionX();
                    int posicionHechoX = posX1 + ( (posX2 - posX1) / 2);
                    nodoHecho1.setPosicionX(posicionHechoX);
                    nodoHecho1.setPosicionY(margenY);
                    nodoHecho1.setPosicionXDescripcion((int) (posicionHechoX + (diametroHechos / 2.6)));
                    nodoHecho1.setPosicionYDescripcion((int) (margenY + (diametroHechos / 1.6)));
                    margenX = posicionDeReglaEnX;
                }else{
                    nodoHecho1.setPosicionX(margenX - retrocesoHechoEnX);
                    nodoHecho1.setPosicionY(margenY);
                    nodoHecho1.setPosicionXDescripcion((int) (margenX - retrocesoHechoEnX + (diametroHechos / 2.6)));
                    nodoHecho1.setPosicionYDescripcion((int) (margenY + (diametroHechos / 1.6)));
                    margenX += separacionHechos;
                }
            }
        }
        return margenX;
    }

    public static int getDiametroHechos() {
        return diametroHechos;
    }

    public static void setDiametroHechos(int diametroHechos) {
        Calculos.diametroHechos = diametroHechos;
    }

    public static int getPerimetroRegla() {
        return perimetroRegla;
    }

    public static void setPerimetroRegla(int perimetroRegla) {
        Calculos.perimetroRegla = perimetroRegla;
    }

    public static int getSeparacionHechos() {
        return separacionHechos;
    }

    public static void setSeparacionHechos(int separacionHechos) {
        Calculos.separacionHechos = separacionHechos;
    }

    public static int getDiferenciaDiametroPerimetro() {
        return diferenciaDiametroPerimetro;
    }

    public static void setDiferenciaDiametroPerimetro(int diferenciaDiametroPerimetro) {
        Calculos.diferenciaDiametroPerimetro = diferenciaDiametroPerimetro;
    }

    public static int getSeparacionReglas() {
        return separacionReglas;
    }

    public static void setSeparacionReglas(int separacionReglas) {
        Calculos.separacionReglas = separacionReglas;
    }

    public static int getRadioHechos() {
        return radioHechos;
    }

    public static void setRadioHechos(int radioHechos) {
        Calculos.radioHechos = radioHechos;
    }

    public static int getLadoRegla() {
        return ladoRegla;
    }

    public static void setLadoRegla(int ladoRegla) {
        Calculos.ladoRegla = ladoRegla;
    }
    
    
}
