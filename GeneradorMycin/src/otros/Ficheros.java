/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gabrielguevara
 */
public class Ficheros {
    
    public static void crearArchivo(String path, String nombre,String datos){
        FileWriter fichero = null;
        PrintWriter writer = null;
        try {
            fichero = new FileWriter(path+nombre);
            writer = new PrintWriter(fichero);
            writer.println(datos);
            writer.close();
            fichero.close();
        } catch (IOException ex) {
            Logger.getLogger(Ficheros.class.getName()).log(Level.SEVERE, null, ex);
            writer.close();
        }
    }
    
    public static void crearArchivoUTF8(String path, String nombre,String datos){
        OutputStreamWriter ew = null;
        BufferedWriter out = null;
        try {
            ew  = new OutputStreamWriter(new FileOutputStream(path+nombre), "utf-8");
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path+nombre), "utf-8"));
            out.write(datos);
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(Ficheros.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
