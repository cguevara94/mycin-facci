/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import modelos.Nodo;

/**
 *
 * @author gabrielguevara
 */
public class ManipulacionNodos {
    
    public static Nodo[] unirNodosAleatoriamente(Nodo[] nodosA, Nodo[] nodosB)
    {
        int tamañoNodoA = nodosA.length;
        int tamañoNodoB = nodosB.length;
        int tamañoNodoResultante = tamañoNodoA + tamañoNodoB;
        Nodo[] nodoResultante = new Nodo[tamañoNodoResultante];
        
        añadirNodoAleatoriamente(nodosA, nodoResultante);
        añadirNodoAleatoriamente(nodosB, nodoResultante);
        
        return nodoResultante;
    }
    
    public static Nodo[] unirNodos(Nodo[] nodosA, Nodo[] nodosB)
    {
        int tamañoNodoA = nodosA.length;
        int tamañoNodoB = nodosB.length;
        int tamañoNodoResultante = tamañoNodoA + tamañoNodoB;
        Nodo[] nodoResultante = new Nodo[tamañoNodoResultante];
        int posición = 0;
        for(int i = 0; i < tamañoNodoA; i++){
            nodoResultante[posición] = nodosA[i];
            posición++;
        }
        
        for(int i = 0; i < tamañoNodoB; i++){
            nodoResultante[posición] = nodosB[i];
            posición++;
        }
        
        return nodoResultante;
    }
    
    public static void añadirNodoAleatoriamente(Nodo[] nodosA, Nodo[] nodosB)
    {
        int tamañoNodoA = nodosA.length;
        int posición;
        for (int i = 0; i < tamañoNodoA; i++) 
        {
            posición = Operaciones.obtenerPosiciónVacia(nodosB);
            nodosB[posición] = nodosA[i];
        }
    }
    
}
