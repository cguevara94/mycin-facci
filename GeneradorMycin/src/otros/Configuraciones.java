/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import javax.xml.bind.annotation.XmlRootElement;
import parametros.*;

/**
 *
 * @author gabrielguevara
 */
@XmlRootElement
public class Configuraciones {
    
    static ParametrosDeArbol parametrosArbol;
    static ParametrosDeNivel parametrosNivel;
    static FactoresDeCerteza factoresDeCerteza;
    static String rutaExportacion;

    public Configuraciones() {
        
    }

    public ParametrosDeArbol getParametrosArbol() {
        return parametrosArbol;
    }

    public void setParametrosArbol(ParametrosDeArbol parametrosArbol) {
        Configuraciones.parametrosArbol = parametrosArbol;
    }

    public ParametrosDeNivel getParametrosNivel() {
        return parametrosNivel;
    }

    public void setParametrosNivel(ParametrosDeNivel parametrosNivel) {
        Configuraciones.parametrosNivel = parametrosNivel;
    }

    public FactoresDeCerteza getFactoresDeCerteza() {
        return factoresDeCerteza;
    }

    public void setFactoresDeCerteza(FactoresDeCerteza factoresDeCerteza) {
        Configuraciones.factoresDeCerteza = factoresDeCerteza;
    }
    
    public String getRutaExportacion() {
        return rutaExportacion;
    }

    public void setRutaExportacion(String rutaExportacion) {
        Configuraciones.rutaExportacion = rutaExportacion;
    }
    
    
    
}
