/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import java.util.ArrayList;
import modelos.Arbol;
import modelos.Nivel;
/**
 *
 * @author gabrielguevara
 */
public class ValidacionCreaciones {
    
    private static boolean seDebeDescartar = false;
    
//    public static boolean creaReglasDeMás(SolicitudArbol solicitudArbol, int cantidadReglas){
//        boolean creaDeMás = cantidadReglas > solicitudArbol.getCantidadReglasACrear();
//        return creaDeMás;
//    }
//    
//    public static boolean creaEvidenciasDeMás(SolicitudArbol solicitudArbol, int cantidadEvidencias){
//        boolean creaDeMás = cantidadEvidencias > solicitudArbol.getCantidadEvidenciasACrear();
//        return creaDeMás;
//    }
//    
//    public static boolean creaHipótesisDeMás(SolicitudArbol solicitudArbol, int cantidadHipótesis){
//        boolean creaDeMás = cantidadHipótesis > solicitudArbol.getCantidadHipótesisACrear();
//        return creaDeMás;
//    }
//    
//    public static boolean permiteMantenerMinimoReglas(SolicitudArbol solicitudArbol, int cantidadReglas){
//        int diferenciaReglas = solicitudArbol.getCantidadReglasACrear() - cantidadReglas;
//        int nivelesRestantes = (solicitudArbol.getCantidadNivelesReglasACrear() - 1);
//        int reglasRestantesPorNivel = nivelesRestantes > 0 ? ( diferenciaReglas / nivelesRestantes ) : 0;
//        boolean permiteElMinimo = ( reglasRestantesPorNivel >= diferenciaReglas && diferenciaReglas >= 0);
//        return permiteElMinimo;
//    }
//    
//    public static boolean permiteMantenerMinimoEvidencias(SolicitudArbol solicitudArbol, int cantidadEvidencias){
//        int diferenciaEvidencias = solicitudArbol.getCantidadEvidenciasACrear() - cantidadEvidencias;
//        int nivelesRestantes = (solicitudArbol.getCantidadNivelesEvidenciasACrear() - 1);
//        int evidenciasRestantes = nivelesRestantes > 0 ? (diferenciaEvidencias / nivelesRestantes ) : 0;
//        boolean permiteElMinimo = ( evidenciasRestantes >= diferenciaEvidencias && diferenciaEvidencias >= 0);
//        return permiteElMinimo;
//    }
//    
//    public static boolean permiteMantenerMinimoHipótesis(SolicitudArbol solicitudArbol, int cantidadHipótesis){
//        int diferenciaHipótesis = solicitudArbol.getCantidadHipótesisACrear() - cantidadHipótesis;
//        int nivelesRestantes = solicitudArbol.getCantidadNivelesHipótesisACrear() - 1;
//        int hipótesisRestantes = nivelesRestantes > 0 ? (diferenciaHipótesis /  nivelesRestantes) : 0;
//        boolean permiteElMinimo = ( hipótesisRestantes >= diferenciaHipótesis && diferenciaHipótesis >= 0);
//        return permiteElMinimo;
//    }

    public static boolean isSeDebeDescartar() {
        return seDebeDescartar;
    }

    public static void setSeDebeDescartar(boolean seDebeDescartar) {
        ValidacionCreaciones.seDebeDescartar = seDebeDescartar;
    }

    
}
