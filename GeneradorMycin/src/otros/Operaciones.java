/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import java.util.Random;

/**
 *
 * @author gabrielguevara
 */
public class Operaciones {
    
    public static int obtenerUnValorEntreLimites(int limiteSuperior, int limiteInferior){
        Random azar = new Random();
        
        int limite = limiteSuperior - limiteInferior + 1;
        int númeroAlAzar = azar.nextInt(limite) + limiteInferior;
        
        return númeroAlAzar;
    }
    
    public static double obtenerUnValorEntreLimites(double limiteSuperior, double limiteInferior){
        Random azar = new Random();
        
        //double limite = limiteSuperior - limiteInferior + 1;
        //double númeroAlAzar = azar.nextInt(limite) + limiteInferior;
        
        double númeroAlAzar;
        int signo;
        boolean seráNegativo;
        do
        {
            númeroAlAzar = azar.nextDouble();
            signo = azar.nextInt(3);
            seráNegativo = ( signo == 0 );
            númeroAlAzar *= seráNegativo ? -1 : 1;
            númeroAlAzar = redondear(númeroAlAzar, 1);
        }while( númeroAlAzar > limiteSuperior || númeroAlAzar < limiteInferior);
        
        return númeroAlAzar;
    }
    
    public static double redondear( double numero, int decimales ) {
        return Math.round(numero*Math.pow(10,decimales))/Math.pow(10,decimales);
    }
    
    public static int obtenerPosiciónEnArreglo(int tamañoDelArreglo){
        Random azar = new Random();
        
        int númeroAlAzar = azar.nextInt(tamañoDelArreglo);
        
        return númeroAlAzar;
    }
    
    public static int sortearAND_OR(){
        Random azar = new Random();
        
        int númeroAlAzar = azar.nextInt(2);
        
        return númeroAlAzar;
    }
    
    public static int obtenerPosiciónVacia(Object[] arreglo)
    {
        int posición = Operaciones.obtenerPosiciónEnArreglo(arreglo.length);
        while( arreglo[posición] != null )
        {
            posición = Operaciones.obtenerPosiciónEnArreglo(arreglo.length);
        }
        return posición;
    }
    
}
