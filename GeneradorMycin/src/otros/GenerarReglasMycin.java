/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import java.util.ArrayList;
import java.util.Arrays;
import modelos.Arbol;
import modelos.Nivel;
import modelos.Nodo;
import modelos.NodoHipotesis;
import modelos.NodoRegla;
import modelos.ReglaMycin;

/**
 *
 * @author gabrielguevara
 */
public class GenerarReglasMycin {
    
    public static String obtenerTesisMycin(ArrayList<ReglaMycin> reglasMycin){
        String reglasString = "";
        ReglaMycin reglaMycin = reglasMycin.get(0);
        reglasString = obtenerReglaString(reglaMycin);
        for(int i = 1; i < reglasMycin.size();i++){
            reglaMycin = reglasMycin.get(i);
            reglasString = String.format("%s\n%s", reglasString,obtenerReglaString(reglaMycin));
        }
        
        return reglasString;
    }
    
    public static String obtenerReglaString(ReglaMycin reglaMycin){
        String reglaString = "";
        String operadorLógico = traducirOperador(reglaMycin.getRegla().getOperadorLógico());
        Nodo[] entradas = reglaMycin.getEntradas();
        reglaString += String.format("%s", entradas[0].getDescripción());
        for (int j = 1; j < entradas.length; j++) {
            reglaString = String.format("%s %s %s", reglaString, operadorLógico, entradas[j].getDescripción());
        }
        reglaString = String.format("%s %s", reglaString, "->");
        reglaString = String.format("%s %s", reglaString,reglaMycin.getHipótesis().getDescripción());
        return reglaString;
    }
    
    private static String traducirOperador(int operadorLógico){
        String operador = null;
        switch(operadorLógico){
            case NodoRegla.OPERADOR_AND:
                operador = "^";
                break;
                
            case NodoRegla.OPERADOR_OR:
                operador = "v";
                break;
            
            default:
                operador = "Regla Sin Operador";
                break;
        }
        return operador;
    }
    
    public static ArrayList<ReglaMycin> obtenerReglasMycin(Arbol arbol){
        ArrayList<ReglaMycin> reglasMycin = new ArrayList<>();
        ArrayList<Nivel> niveles = arbol.getNiveles();
        int totalNiveles = arbol.getCantidadNiveles();
        for (int i = totalNiveles-2; i > 0; i-=2) {
            Nivel nivelReglas = niveles.get(i);
            Nivel nivelEntrante = niveles.get(i+1);
            Nivel nivelSaliente = niveles.get(i-1);
            Nodo[] nodosRegla = nivelReglas.getNodos();
            Nodo[] nodosEntrantes = nivelEntrante.getNodos();
            Nodo[] nodosSalientes = nivelSaliente.getNodos();
            NodoRegla[] reglas = Arrays.copyOf(nodosRegla, nodosRegla.length, modelos.NodoRegla[].class);
            int primerEntradaRegla = 0;
            int ultimaEntradaRegla = 0;
            for(int j = 0; j < reglas.length; j++){
                int númeroEntradas = reglas[j].getNúmeroDeEntradas();
                Nodo[] entradasReglaMycin = new Nodo[númeroEntradas];
                primerEntradaRegla = ultimaEntradaRegla;
                ultimaEntradaRegla += númeroEntradas;
                int asignaciónMycin = 0;
                for(int k = primerEntradaRegla; k < ultimaEntradaRegla; k++){
                    entradasReglaMycin[asignaciónMycin] = nodosEntrantes[k];
                    asignaciónMycin++;
                }
                modelos.ReglaMycin reglaMycin = new ReglaMycin(entradasReglaMycin, reglas[j], (NodoHipotesis) nodosSalientes[reglas[j].getNodoSaliente()]);
                reglasMycin.add(reglaMycin);
            }
        }
        return reglasMycin;
    }
    
    public static String obtenerFactorezDeCerteza(Arbol arbol){
        return String.format("%s\n\n%s", obtenerFactoresDeCertezaEvidencias(arbol), obtenerFactoresDeCertezaReglas(arbol));
    }
    
    public static String obtenerFactoresDeCertezaEvidencias(Arbol arbol)
    {
        String factoresPorEvidencia = "Evidencias:\n";
        ArrayList<Nivel> niveles = arbol.getNiveles();
        int cantidadNiveles = niveles.size();
        for(int i = cantidadNiveles-1; i > 0; i-=2)
        {
            Nivel nivel = niveles.get(i);
            Nodo[] nodos = nivel.getNodos();
            int cantidadNodos = nodos.length;
            for(int j = 0; j < cantidadNodos; j++)
            {
                Nodo nodo = nodos[j];
                if(nodo.getNúmeroDeEntradas() < 0){
                    factoresPorEvidencia = String.format("%s\n%s = %.1f", factoresPorEvidencia, nodo.getDescripción(),
                            nodo.getFactorDeCerteza());
                }
            }
        }
        return factoresPorEvidencia;
    }
    
    public static String obtenerFactoresDeCertezaReglas(Arbol arbol)
    {
        String factoresPorRegla = "Reglas:\n";
        ArrayList<Nivel> niveles = arbol.getNiveles();
        int cantidadNiveles = niveles.size();
        for(int i = cantidadNiveles-2; i > 0; i-=2)
        {
            Nivel nivel = niveles.get(i);
            Nodo[] nodos = nivel.getNodos();
            int cantidadNodos = nodos.length;
            for(int j = 0; j < cantidadNodos; j++)
            {
                Nodo nodo = nodos[j];
                factoresPorRegla = String.format("%s\n%s = %.1f", factoresPorRegla, nodo.getDescripción(), nodo.getFactorDeCerteza());
            }
        }
        return factoresPorRegla;
    }
}
