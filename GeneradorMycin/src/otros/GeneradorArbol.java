/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import modelos.Arbol;

/**
 *
 * @author gabrielguevara
 */
public class GeneradorArbol {
    
    public static Arbol generarArbol(){
        ValidacionCreaciones.setSeDebeDescartar(true);
        Arbol arbol = null;
        int iteraciones = 0;
        while(ValidacionCreaciones.isSeDebeDescartar()){
            arbol = CreacionArbol.crearArbol();
            CreacionNiveles.crearNivelesArbol(arbol);
            iteraciones++;
            if(iteraciones == 100) {ValidacionCreaciones.setSeDebeDescartar(true); System.err.println("No se pudo generar"); break;}
            else{ ValidacionCreaciones.setSeDebeDescartar(false); }
        }
        Descripciones.llenarDescripciones(arbol);
//        ImpresionDetalles.mostratDetalleNodos(arbol);
        return arbol;
    }
    
}
