/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.swing.JOptionPane;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author gabrielguevara
 */
public class ArchivoDeConfiguracion {
    
    public void guardarConfiguracion() throws JAXBException, FileNotFoundException, IOException, URISyntaxException{
        JAXBContext contexto = JAXBContext.newInstance(Configuraciones.class);
        
        Marshaller marshaller = contexto.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        
        String rutaJar = ArchivoDeConfiguracion.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
        rutaJar = rutaJar.substring(0, rutaJar.lastIndexOf('/')+1);
        File archivo = new File(rutaJar+System.getProperty("file.separator")+"configuraciones.xml");
        
        FileOutputStream fos = new FileOutputStream(archivo);
        
        Configuraciones config = new Configuraciones();
        
        marshaller.marshal(config, fos);
        
    }
    
    public void cargarConfiguracion() throws JAXBException, URISyntaxException, IOException{
        JAXBContext contexto = JAXBContext.newInstance(Configuraciones.class);
        
        Marshaller marshaller = contexto.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        
        String rutaJar = ArchivoDeConfiguracion.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
        rutaJar = rutaJar.substring(0, rutaJar.lastIndexOf('/')+1);
        File archivo = new File(rutaJar+System.getProperty("file.separator")+"configuraciones.xml");
        
        Unmarshaller unmarshaller = contexto.createUnmarshaller();
        Configuraciones config = (Configuraciones) unmarshaller.unmarshal(archivo);
    }
    
    public boolean existeArchivoDeConfiguracion() throws URISyntaxException{
        String rutaJar = ArchivoDeConfiguracion.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
        rutaJar = rutaJar.substring(0, rutaJar.lastIndexOf('/')+1);
        File archivo = new File(rutaJar+System.getProperty("file.separator")+"configuraciones.xml");
        return archivo.exists();
    }
    
}
