/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otros;

import java.util.ArrayList;
import modelos.Arbol;
import modelos.Nivel;
import modelos.Nodo;

/**
 *
 * @author gabrielguevara
 */
public class Descripciones {
 
    public static void llenarDescripciones(Arbol arbol){
        ArrayList<Nivel> niveles = arbol.getNiveles();
        Nivel nivel = niveles.get(0);
        Nodo[] nodos = nivel.getNodos();
        Nodo nodo = nodos[0];
        nodo.setDescripción("H");
        int cantidadNiveles = niveles.size();
        int numeraciónEvidencias = 1;
        int numeraciónReglas = 1;
        int numeraciónHipótesis = 1;
        for(int i = cantidadNiveles-1; i > 0; i--){
            nivel = niveles.get(i);
            nodos = nivel.getNodos();
            int cantidadNodos = nodos.length;
            for(int j = 0; j < cantidadNodos; j++){
                nodo = nodos[j];
                if( i%2 == 0){
                    if(nodo.getNúmeroDeEntradas() > -1){
                        nodo.setDescripción("H"+numeraciónHipótesis);
                        numeraciónHipótesis++;
                    }else{
                        nodo.setDescripción("E"+numeraciónEvidencias);
                        numeraciónEvidencias++;
                    }
                }else{
                    nodo.setDescripción("R"+numeraciónReglas);
                    numeraciónReglas++;
                }
            }
        }
        arbol.setCantidadEvidencias(numeraciónEvidencias-1);
        arbol.setCantidadHipótesis(numeraciónHipótesis);
        arbol.setCantidadReglas(numeraciónReglas - 1);
    }
    
}
