/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycin;

import modelos.Arbol;
import otros.CreacionNiveles;
import otros.Descripciones;
import otros.GeneradorArbol;
import otros.GenerarReglasMycin;
import otros.ImpresionDetalles;

/**
 *
 * @author gabrielguevara
 */
public class Mycin {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Arbol arbol = GeneradorArbol.generarArbol();
        ImpresionDetalles.mostratDetalleNodos(arbol);
        System.out.println( GenerarReglasMycin.obtenerTesisMycin(GenerarReglasMycin.obtenerReglasMycin(arbol)));
    }
    
}
