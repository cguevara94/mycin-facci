/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import otros.Operaciones;

/**
 *
 * @author gabrielguevara
 */
public class NivelReglas extends Nivel{

    public NivelReglas(int máximoReglasNivel, int mínimoReglasNivel) {
        super(Operaciones.obtenerUnValorEntreLimites(máximoReglasNivel, mínimoReglasNivel), 0, 0);
    } 
    
}
