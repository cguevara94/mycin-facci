/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author gabrielguevara
 */
public class ReglaMycin {
    
    Nodo[] entradas;
    NodoRegla regla;
    NodoHipotesis hipótesis;

    public ReglaMycin(Nodo[] entradas, NodoRegla regla, NodoHipotesis hipótesis) {
        this.entradas = entradas;
        this.regla = regla;
        this.hipótesis = hipótesis;
    }

    public Nodo[] getEntradas() {
        return entradas;
    }

    public void setEntradas(Nodo[] entradas) {
        this.entradas = entradas;
    }

    public NodoRegla getRegla() {
        return regla;
    }

    public void setRegla(NodoRegla regla) {
        this.regla = regla;
    }

    public NodoHipotesis getHipótesis() {
        return hipótesis;
    }

    public void setHipótesis(NodoHipotesis hipótesis) {
        this.hipótesis = hipótesis;
    }
    
    
    
}
