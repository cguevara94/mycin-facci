/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import otros.Operaciones;
import java.util.ArrayList;
import otros.ManipulacionArbol;


/**
 *
 * @author gabrielguevara
 */
public class Arbol {
    
    private int cantidadNiveles;
    private int cantidadNivelesReglas;
    private int cantidadNivelesHipótesis;
    private int cantidadNivelesEvidencias;
    private int cantidadReglas;
    private int cantidadEvidencias;
    private int cantidadHipótesis;
    private ArrayList<Nivel> niveles;

    public Arbol(int númeroNivelesArbol){
        
        cantidadNiveles = númeroNivelesArbol;
        cantidadNivelesEvidencias = ManipulacionArbol.calcularNivelesEvidenciasReglas(númeroNivelesArbol);
        cantidadNivelesReglas = cantidadNivelesEvidencias;
        cantidadNivelesHipótesis = ManipulacionArbol.calcularNivelesHipótesis(númeroNivelesArbol);
        
        niveles = new ArrayList<>();
    }
    
    private void ajustarHipotesisReglas(int máximoHipótesisArbol, int mínimoHipótesisArbol, int máximoReglasArbol, int mínimoReglasArbol){
        do{
            cantidadHipótesis = Operaciones.obtenerUnValorEntreLimites(máximoHipótesisArbol, mínimoHipótesisArbol);
            cantidadReglas = Operaciones.obtenerUnValorEntreLimites(máximoReglasArbol, mínimoReglasArbol);
        }while(cantidadHipótesis >= cantidadReglas);
    }
    
    public int getCantidadNiveles() {
        return cantidadNiveles;
    }

    public void setCantidadNiveles(int cantidadNiveles) {
        this.cantidadNiveles = cantidadNiveles;
    }

    public int getCantidadReglas() {
        return cantidadReglas;
    }

    public void setCantidadReglas(int cantidadReglas) {
        this.cantidadReglas = cantidadReglas;
    }

    public int getCantidadEvidencias() {
        return cantidadEvidencias;
    }

    public void setCantidadEvidencias(int cantidadEvidencias) {
        this.cantidadEvidencias = cantidadEvidencias;
    }

    public int getCantidadHipótesis() {
        return cantidadHipótesis;
    }

    public void setCantidadHipótesis(int cantidadHipótesis) {
        this.cantidadHipótesis = cantidadHipótesis;
    }

    public ArrayList<Nivel> getNiveles() {
        return niveles;
    }

    public void setNiveles(ArrayList<Nivel> niveles) {
        this.niveles = niveles;
    }

    public int getCantidadNivelesReglas() {
        return cantidadNivelesReglas;
    }

    public void setCantidadNivelesReglas(int cantidadNivelesReglas) {
        this.cantidadNivelesReglas = cantidadNivelesReglas;
    }

    public int getCantidadNivelesHipótesis() {
        return cantidadNivelesHipótesis;
    }

    public void setCantidadNivelesHipótesis(int cantidadNivelesHipótesis) {
        this.cantidadNivelesHipótesis = cantidadNivelesHipótesis;
    }

    public int getCantidadNivelesEvidencias() {
        return cantidadNivelesEvidencias;
    }

    public void setCantidadNivelesEvidencias(int cantidadNivelesEvidencias) {
        this.cantidadNivelesEvidencias = cantidadNivelesEvidencias;
    }
    
}
