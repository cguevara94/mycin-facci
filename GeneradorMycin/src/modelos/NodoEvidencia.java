    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import otros.Operaciones;

/**
 *
 * @author gabrielguevara
 */
public class NodoEvidencia extends Nodo{
    
    public NodoEvidencia(double máximoFactorCerteza, double mínimoFactorCerteza) {
        super( Operaciones.obtenerUnValorEntreLimites(máximoFactorCerteza, mínimoFactorCerteza) );
        super.setNúmeroDeEntradas( -1 );
    }
    
}
