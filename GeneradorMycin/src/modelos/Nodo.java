/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import otros.Operaciones;

/**
 *
 * @author gabrielguevara
 */
public abstract class Nodo {
    
    protected int númeroDeEntradas = -1;
    protected int númeroDeEntradasDisponibles = -1;
    protected int nodoSaliente;
    protected double factorDeCerteza;
    protected String descripción;
    protected int posicionX = 0;
    protected int posicionY = 0;
    protected int posicionXDescripcion = 0;
    protected int posicionYDescripcion = 0;

    public Nodo(double factorDeCerteza)
    {
        this.factorDeCerteza = factorDeCerteza;
    }

    public int getNúmeroDeEntradas() {
        return númeroDeEntradas;
    }

    public void setNúmeroDeEntradas(int númeroDeEntradas) {
        this.númeroDeEntradas = númeroDeEntradas;
    }

    public int getNúmeroDeEntradasDisponibles() {
        return númeroDeEntradasDisponibles;
    }

    public void setNúmeroDeEntradasDisponibles(int númeroDeEntradasDisponibles) {
        this.númeroDeEntradasDisponibles = númeroDeEntradasDisponibles;
    }

    public int getNodoSaliente() {
        return nodoSaliente;
    }

    public void setNodoSaliente(int nodoSaliente) {
        this.nodoSaliente = nodoSaliente;
    }

    public double getFactorDeCerteza() {
        return factorDeCerteza;
    }

    public void setFactorDeCerteza(double factorDeCerteza) {
        this.factorDeCerteza = factorDeCerteza;
    }

    public String getDescripción() {
        return descripción;
    }

    public void setDescripción(String descripción) {
        this.descripción = descripción;
    }

    public int getPosicionX() {
        return posicionX;
    }

    public void setPosicionX(int posicionX) {
        this.posicionX = posicionX;
    }

    public int getPosicionY() {
        return posicionY;
    }

    public void setPosicionY(int posicionY) {
        this.posicionY = posicionY;
    }

    public int getPosicionXDescripcion() {
        return posicionXDescripcion;
    }

    public void setPosicionXDescripcion(int posicionXDescripcion) {
        this.posicionXDescripcion = posicionXDescripcion;
    }

    public int getPosicionYDescripcion() {
        return posicionYDescripcion;
    }

    public void setPosicionYDescripcion(int posicionYDescripcion) {
        this.posicionYDescripcion = posicionYDescripcion;
    }
    
    

}
