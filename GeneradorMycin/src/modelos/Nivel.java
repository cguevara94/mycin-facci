/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import otros.Operaciones;
import java.util.Arrays;
import javax.swing.JOptionPane;

/**
 *
 * @author gabrielguevara
 */
public class Nivel {
    
    private int cantidadReglas;
    private int cantidadEvidencias;
    private int cantidadHipótesis;
    private int cantidadEvidenciasMásHipótesis;
    private Nodo[] nodos;
    
    public Nivel(int cantidadReglas, int cantidadEvidencias, int cantidadHipótesis){
        this.cantidadReglas = cantidadReglas;
        this.cantidadEvidencias = cantidadEvidencias;
        this.cantidadHipótesis = cantidadHipótesis;
        this.cantidadEvidenciasMásHipótesis = cantidadEvidencias + cantidadHipótesis;
        nodos = new Nodo[cantidadReglas + cantidadEvidencias + cantidadHipótesis];
    }
    
    public Object extraerNodo(int posiciónNodo){
        Object nodo = this.nodos[posiciónNodo];
        elimilarNodo(posiciónNodo);
        return nodo;
    }
    
    public void agregarNodo(Nodo nodo)
    {
        agregarNodo(nodo, this.nodos.length);
    }
    
    public void agregarNodo(Nodo nodo, int posiciónNodo){
        Nodo[] nodos = new Nodo[this.nodos.length+1];
        int posArrOriginal = 0;
        for(int posArrAlargado = 0; posArrAlargado < nodos.length; posArrAlargado++)
        {
            if(posArrAlargado == posiciónNodo){
                nodos[posArrAlargado] = nodo;
            }
            else
            {
                nodos[posArrAlargado] = this.nodos[posArrOriginal];
                posArrOriginal++;
            }
        }
        this.nodos = nodos;
    }
    
    public void elimilarNodo(int posiciónNodo){
        int ancho = this.nodos.length;
        Nodo[] nodos = new Nodo[ancho-1];
        int posArrReducido = 0;
        for(int posArrOriginal = 0; posArrOriginal < ancho;posArrOriginal++)
        {
            if(posArrOriginal != posiciónNodo){
                nodos[posArrReducido] = this.nodos[posArrOriginal];
                posArrReducido++;
            }
        }
        this.nodos = nodos;
    }

    public int getCantidadReglas() {
        return cantidadReglas;
    }

    public void setCantidadReglas(int cantidadReglas) {
        this.cantidadReglas = cantidadReglas;
    }

    public int getCantidadEvidencias() {
        return cantidadEvidencias;
    }

    public void setCantidadEvidencias(int cantidadEvidencias) {
        this.cantidadEvidencias = cantidadEvidencias;
    }

    public int getCantidadHipótesis() {
        return cantidadHipótesis;
    }

    public void setCantidadHipótesis(int cantidadHipótesis) {
        this.cantidadHipótesis = cantidadHipótesis;
    }

    public int getCantidadEvidenciasMásHipótesis() {
        return cantidadEvidenciasMásHipótesis;
    }

    public void setCantidadEvidenciasMásHipótesis(int cantidadEvidenciasMásHipótesis) {
        this.cantidadEvidenciasMásHipótesis = cantidadEvidenciasMásHipótesis;
    }

    public Nodo[] getNodos() {
        return nodos;
    }

    public void setNodos(Nodo[] nodos) {
        this.nodos = nodos;
    }

    public int getAnchura() {
        return nodos.length;
    }
}
