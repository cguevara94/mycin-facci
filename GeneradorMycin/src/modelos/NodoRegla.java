/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import otros.Operaciones;

/**
 *
 * @author gabrielguevara
 */
public class NodoRegla extends Nodo{

    public static final int OPERADOR_AND = 0;
    public static final int OPERADOR_OR = 1;
    private int operadorLógico;
    private double valorDeSalida;
    private int númeroDeEvidencias;
    private int númeroDeHipótesis;
    
    public NodoRegla(double máximoFactorCerteza, double mínimoFactorCerteza, int númeroDeEvidencias,
            int númeroDeHipótesis, int operadorLógico) {
        super( Operaciones.obtenerUnValorEntreLimites(máximoFactorCerteza, mínimoFactorCerteza) );
        this.operadorLógico = operadorLógico;
        this.númeroDeEvidencias = númeroDeEvidencias;
        this.númeroDeHipótesis = númeroDeHipótesis;
        super.setNúmeroDeEntradas( númeroDeEvidencias + númeroDeHipótesis );
    }
    
    public int getOperadorLógico() {
        return operadorLógico;
    }

    public void setOperadorLógico(int operadorLógico) {
        this.operadorLógico = operadorLógico;
    }

    public double getValorDeSalida() {
        return valorDeSalida;
    }

    public void setValorDeSalida(double valorDeSalida) {
        this.valorDeSalida = valorDeSalida;
    }

    public int getNúmeroDeEvidencias() {
        return númeroDeEvidencias;
    }

    public void setNúmeroDeEvidencias(int númeroDeEvidencias) {
        this.númeroDeEvidencias = númeroDeEvidencias;
    }

    public int getNúmeroDeHipótesis() {
        return númeroDeHipótesis;
    }

    public void setNúmeroDeHipótesis(int númeroDeHipótesis) {
        this.númeroDeHipótesis = númeroDeHipótesis;
    }
}
